#ifndef DRAW_CHAR_SCENE_HEAD_H
#define DRAW_CHAR_SCENE_HEAD_H


#include <memory>

#include <vector>
#include <string>
#include <memory>
#include <functional>
#include <GL/glut.h>

#include "MathUtil.h"
#include "Camera.h"
#include "Character.h"
#include "DrawUtil.h"
#include <iostream>
#include "DrawUtil.h"
#include "Motion.h"

class cTimer
{
public:
	cTimer() { Init(); };
	virtual void Init()
	{
		mTime = 0; mMaxTime = std::numeric_limits<double>::infinity();
	};
	virtual void Update(double timestep) { mTime += timestep; };
	double mMaxTime;
	double mTime;
};

class cDrawSceneKinChar  
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	inline cDrawSceneKinChar() {   };
	virtual inline ~cDrawSceneKinChar() {};
	virtual void inline Init();
	virtual void inline Update(double time_elapsed); 
	void inline DrawScene();
 
	cTimer mTimer;
	cCamera mCamera; 
    cCharacter * mChar;
	double mTime;
	//--------------------------------------------------------------------
	void inline Draw();
	void inline RestoreView();
	void inline ClearFrame();
	void inline SetupView();
	void inline MouseClick(int button, int state, double x, double y);
	void inline MouseMove(double x, double y);
	//===================================================================
	cMotion mMotion;
	bool inline LoadMotion(const std::string& motion_file);
	void  CharUpdate(double time_step)
	{
		mTime += time_step;
		mMotion.CalcFrame(mTime, mChar->mPose);
	}
		 
};

 
bool cDrawSceneKinChar::LoadMotion(const std::string& motion_file)
{
	cMotion::tParams motion_params;
	motion_params.mMotionFile = motion_file;

	bool succ = mMotion.Load(motion_params);
	//Pose(mTime);
	mMotion.CalcFrame(mTime, mChar->mPose);
	mChar->mPose0 = mChar->mPose;
	mChar->SetOriginPos(Vector4D(0, 0, 0, 0));
	return succ;
}

void cDrawSceneKinChar::Draw()
{
	SetupView();
	ClearFrame();
	DrawScene();
	RestoreView();
}

void cDrawSceneKinChar::SetupView()
{
	cDrawUtil::PushMatrixProj();
	mCamera.SetupGLProj();

	cDrawUtil::PushMatrixView();
	mCamera.SetupGLView();
}


void cDrawSceneKinChar::ClearFrame()
{
	double depth = 1;
	Vector4D col = Vector4D(0.97, 0.97, 1, 0); //GetBkgColor();
	cDrawUtil::ClearDepth(depth);
	cDrawUtil::ClearColor(col);
}

void cDrawSceneKinChar::RestoreView()
{
	cDrawUtil::PopMatrixProj();
	cDrawUtil::PopMatrixView();
}


void cDrawSceneKinChar::MouseClick(int button, int state, double x, double y)
{
	mCamera.MouseClick(button, state, x, y);
}

void cDrawSceneKinChar::MouseMove(double x, double y)
{
	mCamera.MouseMove(x, y);
}


void cDrawSceneKinChar::DrawScene()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	const Vector4D gLineColor = Vector4D(0, 0, 0, 1);
	 Vector4D gFilLColor = Vector4D(0.6f, 0.65f, 0.675f, 1);
	mChar->DrawTree(mChar->mJointMat, mChar->mPose, 0, 0.03, gFilLColor, gLineColor);
}

 
void cDrawSceneKinChar::Init()
{
	mTime = 0;
	const Vector4D gCameraPosition = Vector4D(0, 0, 30, 0);
	const Vector4D gCameraFocus = Vector4D(gCameraPosition[0], gCameraPosition[1], 0.0, 0.0);
	const Vector4D gCameraUp = Vector4D(0, 1, 0, 0);
	const double gViewWidth = 4.5;
	const double gViewHeight = gViewWidth * 9.0 / 16.0;
	const double gViewNearZ = 2;
	const double gViewFarZ = 500;
	const Vector4D gVisOffset = Vector4D(0, 0, 1, 0);
	const Vector4D gCamFocus0 = Vector4D(0, 0.75, 0, 0);
	//==================================================
	
	//-------------------------------------------------
	mTimer.Init();	 
	//-------------------------------------------------
	int proj_mode = 0;
	mCamera = cCamera(proj_mode, gCameraPosition, gCameraFocus, gCameraUp,
		gViewWidth, gViewHeight, gViewNearZ, gViewFarZ);
	//-------------------------------------------------
	Vector4D target_pos = gCamFocus0;// GetDefaultCamFocus();
	target_pos =
	cKinTree::GetRootPos(mChar->mJointMat, mChar->mPose);
	 
	Vector4D cam_pos = gCamFocus0; //GetDefaultCamFocus();
	cam_pos[0] = target_pos[0];
	cam_pos[1] = target_pos[1];

	mCamera.TranslateFocus(cam_pos);
	//-------------------------------------------------
	
}



void cDrawSceneKinChar::Update(double time_elapsed)
{
	mTimer.Update(time_elapsed);
	//mChar->Update(time_elapsed); //UpdateScene(time_elapsed);
	CharUpdate(time_elapsed);
	//UpdateCamera();
	Vector4D track_pos = cKinTree::GetRootPos(mChar->mJointMat, mChar->mPose); //GetCamTrackPos();
	Vector4D cam_focus = mCamera.mFocus;

	double cam_w = mCamera.GetWidth();
	double cam_h = mCamera.GetHeight();
	const double y_pad = std::min(0.5, 0.8 * 0.5 * cam_h);
	const double x_pad = std::min(0.5, 0.8 * 0.5 * cam_w);

	cam_focus[0] = track_pos[0];
	cam_focus[2] = track_pos[2];
	mCamera.TranslateFocus(cam_focus);

}
 
#endif 