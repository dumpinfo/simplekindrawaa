#include <GL/glut.h>
#include <GL/freeglut.h>
 

#include <eigen3/Eigen/Dense>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <stdarg.h>
#include <time.h>
#include "rpfnn.h"
#include "graphdep.h"

#include "worlder.h"
#include "maindisp.h"
#include "Character.h"
#include "rbquat.h"




using namespace std;
static GLboolean should_rotate = GL_TRUE;
using namespace Eigen;


static Options* options = NULL;
/* Phase-Functioned Neural Network */
static PFNN* pfnn = NULL;
/* Joystick */
//static SDL_Joystick* stick = NULL;
/* Camera */
static CameraOrbit* camera = NULL;
static Character* character = NULL;

/* Trajectory */
static Trajectory* trajectory = NULL;
/* IK */
static IK* ik = NULL;
/* Areas */
/* Rendering */
static LightDirectional* light = NULL;
/* Heightmap */
/* Shader */

//_ static Shader* shader_terrain = NULL;
//_ static Shader* shader_terrain_shadow = NULL;
//_ static Shader* shader_character = NULL;
//_ static Shader* shader_character_shadow = NULL;
/* Character */

#include "procaction.h"
#define HeightIdx(X, Y, MAP_SIZE_X)  (X + (Y * MAP_SIZE_X))

void SurfSetColor(float rr,float gg, float bb)
{
    /*RobotMat->SetValue(DIFFUSE, sat_r, sat_g, sat_b, 1.0);
    RobotMat->SetValue(AMBIENT, sat_r, sat_g, sat_b, 1.0);
    RobotMat->SetValue(SPECULAR, 1.0, 1.0, 1.0, 1.0);
    RobotMat->SetValue(SHININESS, 100.0);*/
   
    float  Ambient[4];
	float  Diffuse[4];
    float Specular[4] ={0, 0, 0, 0.0};
    float Emission[4];
    float Shininess =70;
    rr *=1.3;
	gg *=1.3;
	bb *=1.3;
    Ambient[0] = rr; Ambient[1] = gg; Ambient[2] = bb; Ambient[3] = 1;  
	Diffuse[0] = rr; Diffuse[1] = gg; Diffuse[2] = bb; Diffuse[3] = 1;  
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, Ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, Diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Specular);
    //glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Emission);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Shininess);

}

void SurfSetColorRGB(float rr,float gg, float bb)
{
    rr /=255.0;
	gg /=255.0;
	bb /=255.0;
   
    float  Ambient[4];
	float  Diffuse[4];
    float Specular[4] ={0.05, 0.05, 0.05, 1.0};
    float Emission[4];
    float Shininess =70;

    Ambient[0] = rr; Ambient[1] = gg; Ambient[2] = bb; Ambient[3] = 1;  
	Diffuse[0] = rr; Diffuse[1] = gg; Diffuse[2] = bb; Diffuse[3] = 1;  
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, Ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, Diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Specular);
    //glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Emission);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Shininess);

}

void AddLight1(glm::vec3 lightPos) 
{
   GLfloat mat_specular[] = { 1.0, 1000.0, 1.0, 1.0 };
   GLfloat mat_shininess[] = { 00.0 };
   //GLfloat light_position[] = { lightPos[0], lightPos[1]*40, lightPos[2], 0.0 };
   GLfloat light_position[] = { lightPos[0], lightPos[1], lightPos[2], 0.0 };
   //glClearColor (0.0, 0.0, 0.0, 0.0);
   glShadeModel (GL_SMOOTH);

   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
   glLightfv(GL_LIGHT1, GL_POSITION, light_position);

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT1);
   glEnable(GL_DEPTH_TEST);
   
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glTranslatef(lightPos[0], lightPos[1], lightPos[2]);
   glutWireSphere(50,8,8);
   glPopMatrix();
   

   
   float LPos[4];
   glGetLightfv(GL_LIGHT0, GL_POSITION, LPos);
   
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glTranslatef(LPos[0], LPos[1], LPos[2]);
   glutWireSphere(50,8,8);
   glPopMatrix();
}/**/

glm::vec3 normalize(const glm::vec3 &v) {
  float length_of_v = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
  return glm::vec3(v.x / length_of_v, v.y / length_of_v, v.z / length_of_v);
}

float angleBetween(
 glm::vec3 a,
 glm::vec3 b//,glm::vec3 origin 
){
 glm::vec3 da=glm::normalize(a);//glm::vec3 da=glm::normalize(a-origin);
 glm::vec3 db=glm::normalize(b);//glm::vec3 db=glm::normalize(b-origin);
 return  acos(glm::dot(da, db));
}

void DrawPointLink(glm::vec3 vec1, glm::vec3 vec2)
{
	glm::vec3 v = vec1 - vec2;
	float length_of_v = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
	
	glm::vec3 yaxis(0, 0,1);
	
    glm::vec3 res = cross(v, yaxis);
	float angle = angleBetween( v, yaxis);
	
	angle = 180 - angle*180.0/3.1415;
	
	glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glTranslatef(vec1[0], vec1[1], vec1[2]);
	glRotatef(angle, res[0],res[1],res[2]);
	
	SurfSetColorRGB(153,160, 173);
	glutSolidCylinder(5.5,length_of_v,8,8); 
	
	SurfSetColor(0.0,0, 0);
	glutWireCylinder(5.6,length_of_v,8,2); 
    //glTranslatef(LPos[0], LPos[1], LPos[2]);
    //glutSolidSphere(radius,10,10);
    glPopMatrix();
}

void DrawSphere(glm::vec3 LPos, double radius=6)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
    glutSolidSphere(radius,10,10);
    glPopMatrix();
}

void DrawSphereWire(glm::vec3 LPos, double radius=6)
{
	SurfSetColor(0.0,0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
    glutWireSphere(radius+0.4, 8, 6);
    glPopMatrix();
}


void DrawSphereWireT(glm::vec3 LPos, double radius=6)
{
	SurfSetColor(0.0,0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
	glDisable(GL_DEPTH_TEST);
    glutWireSphere(radius, 8, 8);
	glEnable(GL_DEPTH_TEST);
    glPopMatrix();
}

string node_names[17] =
{
"root",
"chest",
"neck",
"right_shoulder",
"right_elbow",
"right_wrist",
"left_shoulder",
"left_elbow",
"left_wrist",
"right_hip",
"right_knee",
"right_ankle",
"right_toe",
"left_hip",
"left_knee",
"left_ankle",
"left_toe"
};

//int x_vel,int y_vel
void myinit();
void disp_trajectory()
{
	glPointSize(1.0 * options->display_scale);
	glBegin(GL_POINTS);
	for (int i = 0; i < Trajectory::LENGTH - 10; i++) {
		glm::vec3 position_c = trajectory->positions[i];
		glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
		glVertex3f(position_c.x, position_c.y + 2.0, position_c.z);
	}
	glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glPointSize(1.0);


	glPointSize(4.0 * options->display_scale);
	glBegin(GL_POINTS);
	for (int i = 0; i < Trajectory::LENGTH; i += 10) {
		glm::vec3 position_c = trajectory->positions[i];
		glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
		glVertex3f(position_c.x, position_c.y + 2.0, position_c.z);
	}
	glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glPointSize(1.0);

	if (options->display_debug_heights) {
		glPointSize(2.0 * options->display_scale);
		glBegin(GL_POINTS);
		for (int i = 0; i < Trajectory::LENGTH; i += 10) {
			glm::vec3 position_r = trajectory->positions[i] + (trajectory->rotations[i] * glm::vec3(trajectory->width, 0, 0));
			glm::vec3 position_l = trajectory->positions[i] + (trajectory->rotations[i] * glm::vec3(-trajectory->width, 0, 0));
			glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
			glVertex3f(position_r.x, heightmap->sample(glm::vec2(position_r.x, position_r.z)) + 2.0, position_r.z);
			glVertex3f(position_l.x, heightmap->sample(glm::vec2(position_l.x, position_l.z)) + 2.0, position_l.z);
		}
		glEnd();
		glColor3f(1.0, 1.0, 1.0);
		glPointSize(1.0);
	}

	glLineWidth(1.0 * options->display_scale);
	glBegin(GL_LINES);
	for (int i = 0; i < Trajectory::LENGTH; i += 10) {
		glm::vec3 base = trajectory->positions[i] + glm::vec3(0.0, 2.0, 0.0);
		glm::vec3 side = glm::normalize(glm::cross(trajectory->directions[i], glm::vec3(0.0, 1.0, 0.0)));
		glm::vec3 fwrd = base + 15.0f * trajectory->directions[i];
		fwrd.y = heightmap->sample(glm::vec2(fwrd.x, fwrd.z)) + 2.0;
		glm::vec3 arw0 = fwrd + 4.0f * side + 4.0f * -trajectory->directions[i];
		glm::vec3 arw1 = fwrd - 4.0f * side + 4.0f * -trajectory->directions[i];
		glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
		glVertex3f(base.x, base.y, base.z);
		glVertex3f(fwrd.x, fwrd.y, fwrd.z);
		glVertex3f(fwrd.x, fwrd.y, fwrd.z);
		glVertex3f(arw0.x, fwrd.y, arw0.z);
		glVertex3f(fwrd.x, fwrd.y, fwrd.z);
		glVertex3f(arw1.x, fwrd.y, arw1.z);
	}
	glEnd();
	glLineWidth(1.0);
	glColor3f(1.0, 1.0, 1.0);
}

void render_human()
{
	/* Render Joints */
	if (1)
		// if (options->display_debug && options->display_debug_joints)
	{
		glDisable(GL_DEPTH_TEST);
		glPointSize(3.0 * options->display_scale);
		glColor3f(0.6, 0.3, 0.4);
		glBegin(GL_POINTS);
		for (int i = 0; i < Character::JOINT_NUM; i++) {
			glm::vec3 pos = character->joint_positions[i];
			glVertex3f(pos.x, pos.y, pos.z);
		}
		glEnd();
		glPointSize(1.0);


		//####################################################
		//####################################################
		character->DrawHumanTree(0);
		//cDrawUtil::PushMatrixView();
		//glTranslatef(character->joint_positions[0].x, character->joint_positions[0].y, character->joint_positions[0].z);
		////character->DrawHumanTree(0);
		//cDrawUtil::SetColor(Vector4D(0.7, 0, 0, 1.0));
		//cDrawUtil::DrawSphere(5);
		//vector<int>  childs;
		//character->get_node_childs(0, childs);
		//for (int i = 0; i < childs.size(); i++)
		//{
		//	cDrawUtil::PushMatrixView();
		//	Matrix4D m = character->joint_local_transform[childs[i]];
		//	cDrawUtil::MultMatrixView(m);
		//	cDrawUtil::DrawSphere(5);
		//	cDrawUtil::PopMatrixView();
		//}
		//cDrawUtil::PopMatrixView();
		//---------------------------------------------------
		glLineWidth(4.0);
		glBegin(GL_LINES);
		glColor3f(0.6, 0.3, 0.4);
		for (int i = 0; i < Character::JOINT_NUM; i++) {
			if (character->joint_parents[i] == -1) continue;

			glm::vec3 pos1 = character->joint_positions[i];
			glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]];

			glVertex3f(pos1.x, pos1.y, pos1.z);
			glVertex3f(pos2.x, pos2.y, pos2.z);

			Vector4D P1(pos1.x, pos1.y, pos1.z, 0);
			Vector4D P2(pos2.x, pos2.y, pos2.z, 0);
			Vector4D P3 = P1 - P2;
			float dist = P3.norm();
			printf("(%s->%s)%02i->%02i##%6.3f,%6.3f,%6.3f||%6.3f,%6.3f,%6.3f||%6.3f//\n",
				node_names[character->joint_parents[i]].c_str(),
				node_names[i].c_str(),
				i,
				character->joint_parents[i], 
				pos1.x, pos1.y, pos1.z,
				pos2.x, pos2.y, pos2.z,
				dist);
		}
		glEnd();
		glLineWidth(1.0);
		glEnable(GL_DEPTH_TEST);

		//=====================================================
		for (int i = 0; i < Character::JOINT_NUM; i++)
		{
			/*if (i == 1)
			{


				glm::vec3 offset, offset1, offset2;

				offset = (character->joint_positions[1] -
					character->joint_positions[0]);
				offset1[0] = offset[0] * 0.5;
				offset1[1] = offset[1] * 0.5;
				offset1[2] = offset[2] * 0.5;
				SurfSetColorRGB(119, 134, 167);
				DrawSphere(character->joint_positions[i] - offset1, 12);
				DrawSphereWire(character->joint_positions[i] - offset1, 12);


				offset = (character->joint_positions[2] -
					character->joint_positions[1]);
				offset2[0] = offset[0] * 0.4;
				offset2[1] = offset[1] * 0.4;
				offset2[2] = offset[2] * 0.4;
				SurfSetColorRGB(119, 134, 167);
				DrawSphere(character->joint_positions[i] + offset2, 16);
				DrawSphereWire(character->joint_positions[i] + offset2, 16);
			}
			if (i == 2)
			{
				//DrawSphere(character->joint_positions[i] + (character->joint_positions[2] -
				 //                                           character->joint_positions[1])/4.0, 13);
				glm::vec3 offset = (character->joint_positions[2] -
					character->joint_positions[1]);
				offset[0] = offset[0] / 1.5;
				offset[1] = offset[1] / 1.5;
				offset[2] = offset[2] / 1.5;
				SurfSetColorRGB(119, 134, 167);;
				DrawSphere(character->joint_positions[i] + offset, 12);
				DrawSphere(character->joint_positions[i], 8);

				DrawSphereWire(character->joint_positions[i] + offset, 12);
				DrawSphereWire(character->joint_positions[i], 8);
			}
			else*/
			{
				SurfSetColorRGB(119, 134, 167);
				DrawSphere(character->joint_positions[i],3);
			}
		}

		/*SurfSetColor(0.5, 0.55, 0.8);
		for (int i = 0; i < Character::JOINT_NUM; i++) {
			if (character->joint_parents[i] == -1) continue;
			if (character->joint_parents[i] == 1)
			{
				glm::vec3 offset = (character->joint_positions[2] -
					character->joint_positions[1]);
				offset[0] = offset[0] / 1.2;
				offset[1] = offset[1] / 1.2;
				offset[2] = offset[2] / 1.2;
				glm::vec3 pos1 = character->joint_positions[i];
				glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]] + offset;
				DrawPointLink(pos1, pos2);
			}
			else
			{
				glm::vec3 pos1 = character->joint_positions[i];
				glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]];
				DrawPointLink(pos1, pos2);
			}
		}*/
		//======================================================
	}
}
void render(int x_vel,int y_vel) {
  
  /* Render Shadows */
  glm::mat4 light_view = glm::lookAt(camera->target + light->position, camera->target, glm::vec3(0,1,0)); 
  glm::mat4 light_proj = glm::ortho(-500.0f, 500.0f, -500.0f, 500.0f, 10.0f, 10000.0f);
  light_view[1]+=2000;

  //glBindFramebuffer(GL_FRAMEBUFFER, light->fbo);
 
  glViewport(0, 0, 1024, 1024);
 // glClearDepth(1.0f);  
  glClear(GL_DEPTH_BUFFER_BIT);
  
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);
  

  glCullFace(GL_BACK);
   
  
  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
  //_ glBindFramebuffer(GL_FRAMEBUFFER, 0);
  
  /* Render Terrain */
 
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  
  glClearDepth(1.0);
  glClearColor(0.67, 0.85, 0.90, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glm::vec3 light_direction = glm::normalize(light->target - light->position);
  
  
  
  //==================================================================
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(camera->view_matrix()));
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(camera->proj_matrix()));
  bool bSwitchSides = false;
  
  int STEP_SIZE  = 2;
  int MAP_SIZE_X = heightmap->w -1;
  int MAP_SIZE_Y = heightmap->h -1;
  int X,Y;
  X = Y = 0;
  int round_f = MAP_SIZE_Y%STEP_SIZE;
  
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  //std::cout<< light->position[0] <<"," << light->position[1] <<"," << light->position[2] <<"," <<"\n";
  //light->position[1]+=2000;
  AddLight1(light->position) ;
  SurfSetColorRGB(197, 196, 182);
  
  
  //glutWireSphere(200,20,20);
  glBegin( GL_TRIANGLE_STRIP );
 
   for ( X = 0; X <= (MAP_SIZE_X-1); X += STEP_SIZE )
	{ float x,y,z,Idx;
		// 判断渲染的面
		if(bSwitchSides)
		{	
			//  遍历所有列
			for ( Y = (MAP_SIZE_Y - round_f); Y > 0; Y -= STEP_SIZE )
			{
				// 获得高程值		
				Idx = HeightIdx(X, Y , heightmap->w); 
				x = heightmap->posns[Idx][0];							
				y = heightmap->posns[Idx][1];	
				z = heightmap->posns[Idx][2];	
                glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);				
				glVertex3f(x, y, z);		

				Idx = HeightIdx( (X + STEP_SIZE), Y , heightmap->w); 
				// 获得高程值		
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z);			
			}
		}
		else
		{
			Y = 0;
			//  遍历所有的行
			for ( Y = 0; Y <= (MAP_SIZE_Y); Y += STEP_SIZE )
			{
				// 获得高程值		
				Idx = HeightIdx( (X + STEP_SIZE), Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z); 

				// 获得高程值
				Idx = HeightIdx(X, Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z);		
			}
		}

		bSwitchSides = !bSwitchSides;
	}
	
  glEnd();/**/
  //========================================================================
   
  glEnable(GL_DEPTH_TEST);
  //#######################################################################
  //#######################################################################
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(camera->view_matrix()));
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(camera->proj_matrix()));
  
  SurfSetColorRGB(38, 76, 35);
  /* Render Crouch Area */
 
  
  /* Render Trajectory */
  
  disp_trajectory();
  
  render_human();
 
  
  /* UI Elements */

  glm::mat4 ui_view = glm::mat4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
  glm::mat4 ui_proj = glm::ortho(0.0f, (float)WINDOW_WIDTH, (float)WINDOW_HEIGHT, 0.0f, 0.0f, 1.0f);  
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(ui_proj));

  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(ui_view));
  
   
  
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
   
   
    //------------AXIS-------------------------
	glDisable(GL_DEPTH_TEST);
	glLineWidth(4);
    glBegin(GL_LINES);
    
    glColor3f(1, 0.0, 0);
    glVertex3f(0,  4, 0);
    glVertex3f(50, 4, 0);
    
    glColor3f(0, 0.0, 1);
    glVertex3f(0,  4, 0);
    glVertex3f(0,  4, 50);
    
	glColor3f(0, 1.0, 0);
    glVertex3f(0,  0, 0);
    glVertex3f(0,  50, 0);
	
    glEnd();
	glEnable(GL_DEPTH_TEST);
   //---------------------------------------
  //glDisable(GL_DEPTH_TEST);
  //glDisable(GL_CULL_FACE);
  //SDL_GL_SwapBuffers( );//<============
   glutSwapBuffers( );
}

//############################################################
static void setup_opengl( int width, int height )
{
    float ratio = (float) width / (float) height;

    /* Our shading model--Gouraud (smooth). */
    glShadeModel( GL_SMOOTH );

    /* Culling. */
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );
    glEnable( GL_CULL_FACE );

    /* Set the clear color. */
    glClearColor( 0, 0, 0, 0 );

    /* Setup our viewport. */
    glViewport( 0, 0, width, height );

    /*
     * Change to the projection matrix and set
     * our viewing volume.
     */
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    /*
     * EXERCISE:
     * Replace this with a call to glFrustum.
     */
    gluPerspective( 60.0, ratio, 1.0, 1024.0 );
}

int x_vel(0), y_vel(0);

static void
key(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27 : glutLeaveMainLoop () ;      break;
    //case 'Q':
    //case 'q': 
	case 'w': y_vel = 30000; break;
    case 'x': y_vel = -30000; break;
	case 'a': x_vel = 30000; break;
    case 'd': x_vel = -30000; break;
	
	case 'q': x_vel =  30000;  y_vel = 30000;break;
	case 'e': x_vel = -30000;  y_vel = 30000;break;
	case 'z': x_vel =  30000;  y_vel =-30000;break;
	case 'c': x_vel = -30000;  y_vel =-30000;break;
	
	case '1': load_world0(); break;
    case '2': load_world1(); break;
    case '3': load_world2(); break;
    case '4': load_world3(); break;
    case '5': load_world4(); break;
    case '6': load_world5(); break;
   

    default:
        break;
    }

    glutPostRedisplay();
}

void display()
{
    pre_render( x_vel, y_vel);
    render(x_vel, y_vel);
	x_vel = y_vel =0;
    post_render();
}

static void
resize(int width, int height)
{
    float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
}
//==============================================================
#define GAP  25 
int main( int argc, char* argv[] )
{
  
//========================================================================
// Resources  
  options = new Options();
  camera = new CameraOrbit();
  light = new LightDirectional();
  
  character = new Character();
  character->load(
    "./network/character_vertices.bin", 
    "./network/character_triangles.bin", 
    "./network/character_parents.bin", 
    "./network/character_xforms.bin");

  character->gCharModel.Init("humanoid3d.txt");

  trajectory = new Trajectory();
  ik = new IK();
  heightmap = new Heightmap();
  areas = new Areas();
  
  pfnn = new PFNN(PFNN::MODE_CONSTANT);// PFNN::MODE_CUBIC, PFNN::MODE_LINEAR
  pfnn->load();
  
  load_world2();
  // Game Loop 
//==================================================================
    int width = 0; int height = 0;
    width = WINDOW_WIDTH;
    height = WINDOW_HEIGHT;
    //bpp = info->vfmt->BitsPerPixel;
	//printf("bpp:%i\n", bpp);
    glutInitWindowSize(WINDOW_WIDTH+GAP*3, WINDOW_HEIGHT+GAP*3);
    //glutInitWindowSize(WINDOW_WIDTH , WINDOW_HEIGHT);
    glutInitWindowPosition(40,40);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
    GLuint window = glutCreateWindow("FreeGLUT PFNN");
	glutReshapeFunc(main_reshape);
    glutDisplayFunc(main_display);
    glutKeyboardFunc(main_keyboard);
	
	glutCreateSubWindow( window, GAP, GAP, WINDOW_WIDTH, WINDOW_HEIGHT );
    myinit();
	 
    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutMainLoop();
     

    //===============================================
  delete options;
  delete camera;
  delete light;
  delete character;
  delete trajectory;
  delete ik;
   
  delete heightmap;
  delete areas;
  delete pfnn;
    return 0;
}

 

void myinit()
{

    GLfloat mat_specular[]={0.2, 0.2, 0.2, 1.0};
    GLfloat mat_diffuse[]={1.0, 1.0, 1.0, 1.0};
    GLfloat mat_ambient[]={1.0, 1.0, 1.0, 1.0};
    GLfloat mat_shininess={100.0};
    GLfloat light_ambient[]={0.0, 0.0, 0.0, 1.0};
    GLfloat light_diffuse[]={1.0, 1.0, 1.0, 1.0};
    GLfloat light_specular[]={1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[]={10.0, 10.0, 10.0, 0.0};

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_DEPTH_TEST);
    //glClearColor (0.0, 0.0, 0.0, 1.0);
    //glColor3f (1.0, 1.0, 1.0);
    glEnable(GL_NORMALIZE); /* automatic normaization of normals */
    glEnable(GL_CULL_FACE); /* eliminate backfacing polygons */
    glCullFace(GL_BACK); 
	
	//===============================================
	//glPointSize(6);
	//glLineWidth(6);

	// The following commands should induce OpenGL to create round points and 
	//	antialias points and lines.  (This is implementation dependent unfortunately, and
	//  may slow down rendering considerably.)
	//  You may comment these out if you wish.
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);	// Make round points, not square points
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);		// Antialias the lines
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}


