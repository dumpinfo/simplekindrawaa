#ifndef DRAW_UTIL_HEAD_H
#define DRAW_UTIL_HEAD_H

#include "MathUtil.h"
#include <GL/glut.h>
#include <GL/glut.h> 
#include <assert.h>
#include <string>
//--------------------------------------------------------------


class cDrawUtil
{
public:
	enum eDrawMode
	{
		eDrawSolid,
		eDrawWire,
		eDrawWireSimple, 
		eDrawMax
	};
	
	static GLUquadricObj* gQuadObj;
	cDrawUtil() { cDrawUtil::gQuadObj = gluNewQuadric(); }
	static void inline InitDrawUtil();
	static void inline DrawBox(const Vector4D& pos, const Vector4D& size, eDrawMode draw_mode = eDrawSolid);
	static void inline DrawSphere(double r,   eDrawMode draw_mode = eDrawSolid);
	static void inline DrawCylinder(double r, double h,  eDrawMode draw_mode = eDrawSolid);
	static void inline DrawCapsule(double r, double h,  eDrawMode draw_mode = eDrawSolid);	 
	
	static void inline ClearColor(const Vector4D& col);
	static void inline ClearDepth(double depth);

	static void inline Translate(const Vector4D& trans);
	static void inline Rotate(double theta, const Vector4D& axis);
	static void inline SetColor(const Vector4D& col);
	static void inline SetLineWidth(double w);
	static void inline GLMultMatrix(const Matrix4D& mat);
     
    static void MultMatrixView(const Matrix4D& mat){GLMultMatrix(mat);};//KHAdd
	static void Rotate(Vector4D&euler) {
		double theta; Vector4D axis;
		cMathUtil::EulerToAxisAngle(euler, axis, theta); Rotate(theta, axis);
	};//KHAdd

    static void PushMatrixView(){glMatrixMode(GL_MODELVIEW);  glPushMatrix();}//KHAdd static
	static void PopMatrixView() {glMatrixMode(GL_MODELVIEW);  glPopMatrix();}//KHAdd static 
	static void PushMatrixProj(){glMatrixMode(GL_PROJECTION); glPushMatrix();};
	static void PopMatrixProj() {glMatrixMode(GL_PROJECTION); glPopMatrix();};
	
	static void LoadIdentityView() { glMatrixMode(GL_MODELVIEW); glLoadIdentity(); };//KHAdd
	static void LoadIdentityProj() { glMatrixMode(GL_PROJECTION); glLoadIdentity(); };//KHAdd
};

 

void cDrawUtil::InitDrawUtil()
{
 
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glFrontFace(GL_CCW);
}


void cDrawUtil::DrawBox(const Vector4D& pos, const Vector4D& size, eDrawMode draw_mode)
{
	Vector4D sw0 = Vector4D(pos[0] - 0.5 * size[0], pos[1] - 0.5 * size[1], pos[2] - 0.5 * size[2], pos[2]);
	Vector4D se0 = Vector4D(pos[0] + 0.5 * size[0], pos[1] - 0.5 * size[1], pos[2] - 0.5 * size[2], pos[2]);
	Vector4D ne0 = Vector4D(pos[0] + 0.5 * size[0], pos[1] + 0.5 * size[1], pos[2] - 0.5 * size[2], pos[2]);
	Vector4D nw0 = Vector4D(pos[0] - 0.5 * size[0], pos[1] + 0.5 * size[1], pos[2] - 0.5 * size[2], pos[2]);

	Vector4D sw1 = Vector4D(pos[0] - 0.5 * size[0], pos[1] - 0.5 * size[1], pos[2] + 0.5 * size[2], pos[2]);
	Vector4D se1 = Vector4D(pos[0] + 0.5 * size[0], pos[1] - 0.5 * size[1], pos[2] + 0.5 * size[2], pos[2]);
	Vector4D ne1 = Vector4D(pos[0] + 0.5 * size[0], pos[1] + 0.5 * size[1], pos[2] + 0.5 * size[2], pos[2]);
	Vector4D nw1 = Vector4D(pos[0] - 0.5 * size[0], pos[1] + 0.5 * size[1], pos[2] + 0.5 * size[2], pos[2]);

	GLenum gl_mode = (draw_mode == eDrawSolid) ? GL_QUADS : GL_LINE_LOOP;
	glTexCoord2d(0, 0);
	glBegin(gl_mode);
	// top
	glNormal3d(0, 1, 0);
	glTexCoord2d(0, 0);
	glVertex3d(nw1[0], nw1[1], nw1[2]);
	glTexCoord2d(1, 0);
	glVertex3d(ne1[0], ne1[1], ne1[2]);
	glTexCoord2d(1, 1);
	glVertex3d(ne0[0], ne0[1], ne0[2]);
	glTexCoord2d(0, 1);
	glVertex3d(nw0[0], nw0[1], nw0[2]);
	glEnd();
	glBegin(gl_mode);
	// bottom
	glNormal3d(0, -1, 0);
	glTexCoord2d(0, 0);
	glVertex3d(sw0[0], sw0[1], sw0[2]);
	glTexCoord2d(1, 0);
	glVertex3d(se0[0], se0[1], se0[2]);
	glTexCoord2d(1, 1);
	glVertex3d(se1[0], se1[1], se1[2]);
	glTexCoord2d(0, 1);
	glVertex3d(sw1[0], sw1[1], sw1[2]);
	glEnd();
	glBegin(gl_mode);
	// front
	glNormal3d(0, 0, 1);
	glTexCoord2d(0, 0);
	glVertex3d(sw1[0], sw1[1], sw1[2]);
	glTexCoord2d(1, 0);
	glVertex3d(se1[0], se1[1], se1[2]);
	glTexCoord2d(1, 1);
	glVertex3d(ne1[0], ne1[1], ne1[2]);
	glTexCoord2d(0, 1);
	glVertex3d(nw1[0], nw1[1], nw1[2]);
	glEnd();
	glBegin(gl_mode);
	// back
	glNormal3d(0, 0, -1);
	glTexCoord2d(0, 0);
	glVertex3d(se0[0], se0[1], se0[2]);
	glTexCoord2d(1, 0);
	glVertex3d(sw0[0], sw0[1], sw0[2]);
	glTexCoord2d(1, 1);
	glVertex3d(nw0[0], nw0[1], nw0[2]);
	glTexCoord2d(0, 1);
	glVertex3d(ne0[0], ne0[1], ne0[2]);
	glEnd();
	glBegin(gl_mode);
	// left
	glNormal3d(-1, 0, 0);
	glTexCoord2d(0, 0);
	glVertex3d(sw0[0], sw0[1], sw0[2]);
	glTexCoord2d(1, 0);
	glVertex3d(sw1[0], sw1[1], sw1[2]);
	glTexCoord2d(1, 1);
	glVertex3d(nw1[0], nw1[1], nw1[2]);
	glTexCoord2d(0, 1);
	glVertex3d(nw0[0], nw0[1], nw0[2]);
	glEnd();
	glBegin(gl_mode);
	// right
	glNormal3d(1, 0, 0);
	glTexCoord2d(0, 0);
	glVertex3d(se1[0], se1[1], se1[2]);
	glTexCoord2d(1, 0);
	glVertex3d(se0[0], se0[1], se0[2]);
	glTexCoord2d(1, 1);
	glVertex3d(ne0[0], ne0[1], ne0[2]);
	glTexCoord2d(0, 1);
	glVertex3d(ne1[0], ne1[1], ne1[2]);
	glEnd();
}

void cDrawUtil::DrawSphere(double r, eDrawMode draw_mode)
{
	int slices = 8; int stacks = 8;
	glPushMatrix();
	cDrawUtil::Rotate(M_PI / 2, Vector4D(1, 0, 0, 0));
	if (draw_mode == eDrawSolid)
	{
		glutSolidSphere(r, slices, stacks);
	}
	else
	{
		glutWireSphere(r, slices, stacks);
	}
	glPopMatrix();
}

void cDrawUtil::DrawCylinder(double r, double h, eDrawMode draw_mode)
{
	int slices = 8;
	GLenum gl_mode = (draw_mode == eDrawSolid) ? GL_TRIANGLE_STRIP : GL_LINE_LOOP;

	glBegin(gl_mode);
	glTexCoord2d(0, 0);
	for (int i = 0; i <= slices; ++i)
	{
		double theta = i * 2 * M_PI / slices;
		double x = r * std::cos(theta);
		double z = r * std::sin(theta);

		Vector4D normal = Vector4D(x, z, 0, 0).normalized();

		glNormal3d(normal[0], normal[1], normal[2]);
		glVertex3d(x, -h * 0.5, z);
		glVertex3d(x, h * 0.5, z);
	}
	glEnd();
}



void cDrawUtil::DrawCapsule(double r, double h, eDrawMode draw_mode)
{
	int slices = 8; int stacks = 8;
	glPushMatrix();
	DrawCylinder(r, h, draw_mode);

	cDrawUtil::Translate(Vector4D(0, h * 0.5, 0, 0));
	DrawSphere(r, draw_mode);//slices, stacks,
	cDrawUtil::Translate(Vector4D(0, -h, 0, 0));
	DrawSphere(r, draw_mode);//slices, stacks,

	glPopMatrix();
}

void cDrawUtil::ClearColor(const Vector4D& col)
{
	glClearColor(static_cast<float>(col[0]), static_cast<float>(col[1]),
		static_cast<float>(col[2]), static_cast<float>(col[3]));
	glClear(GL_COLOR_BUFFER_BIT);
}

void cDrawUtil::ClearDepth(double depth)
{
	glClearDepth(depth);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void cDrawUtil::GLMultMatrix(const Matrix4D& mat)
{
	glMultMatrixd(mat.data());
}



void cDrawUtil::Translate(const Vector4D& trans)
{
	glTranslated(trans[0], trans[1], trans[2]);
}



void cDrawUtil::Rotate(double theta, const Vector4D& axis)
{
	glRotated(theta * gRadiansToDegrees, axis[0], axis[1], axis[2]);
}

void cDrawUtil::SetColor(const Vector4D& col)
{
	glColor4d(col[0], col[1], col[2], col[3]);
}

void cDrawUtil::SetLineWidth(double w)
{
	glLineWidth(static_cast<float>(w));
}


#endif