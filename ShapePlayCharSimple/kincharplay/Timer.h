#ifndef TIMER_COUNT_H
#define TIMER_COUNT_H
#include <string>

class cTimer
{
public: 
	cTimer() { Init(); };
	virtual void Init() 
	{ mTime = 0; mMaxTime = std::numeric_limits<double>::infinity(); };
	virtual void Update(double timestep) { mTime += timestep; };
	double mMaxTime;
	double mTime;
};

#endif