#ifndef MATH_UTIL_HEAD_H
#define MATH_UTIL_HEAD_H

#include <random>
#include <time.h>
#include "Eigen/Dense"
#include "Eigen/StdVector"
#include "Eigen/Geometry"
 

#define M_PI 3.1415926535
const int gInvalidIdx = -1;

typedef Eigen::Vector4d Vector4D;
typedef Eigen::Matrix4d Matrix4D;
typedef Eigen::Quaterniond tQuaternion;

const double gRadiansToDegrees = 57.2957795;
const double gDegreesToRadians = 1.0 / gRadiansToDegrees;
const Vector4D gGravity = Vector4D(0, -9.8, 0, 0);
const double gInchesToMeters = 0.0254;
const double gFeetToMeters = 0.3048;

class cMathUtil
{
public:	 
	 
	static Matrix4D inline TranslateMat(const Vector4D& trans);
	static Matrix4D inline RotateMat(const Vector4D& euler); // euler angles order rot(Z) * rot(Y) * rot(X)
	static Matrix4D inline RotateMat(const Vector4D& axis, double theta);
	static Matrix4D inline RotateMat(const tQuaternion& q);
	static Matrix4D inline InvRigidMat(const Matrix4D& mat);
	static void inline EulerToAxisAngle(const Vector4D& euler, Vector4D& out_axis, double& out_theta);
	static tQuaternion inline VecToQuat(const Vector4D& v);
	 
	
private:
 
};


Matrix4D cMathUtil::TranslateMat(const Vector4D& trans)
{
	Matrix4D mat = Matrix4D::Identity();
	mat(0, 3) = trans[0];
	mat(1, 3) = trans[1];
	mat(2, 3) = trans[2];
	return mat;
}


Matrix4D cMathUtil::RotateMat(const Vector4D& euler)
{
	double x = euler[0];
	double y = euler[1];
	double z = euler[2];

	double x_s = std::sin(x);
	double x_c = std::cos(x);
	double y_s = std::sin(y);
	double y_c = std::cos(y);
	double z_s = std::sin(z);
	double z_c = std::cos(z);

	Matrix4D mat = Matrix4D::Identity();
	mat(0, 0) = y_c * z_c;
	mat(1, 0) = y_c * z_s;
	mat(2, 0) = -y_s;

	mat(0, 1) = x_s * y_s * z_c - x_c * z_s;
	mat(1, 1) = x_s * y_s * z_s + x_c * z_c;
	mat(2, 1) = x_s * y_c;

	mat(0, 2) = x_c * y_s * z_c + x_s * z_s;
	mat(1, 2) = x_c * y_s * z_s - x_s * z_c;
	mat(2, 2) = x_c * y_c;

	return mat;
}

Matrix4D cMathUtil::RotateMat(const Vector4D& axis, double theta)
{
	assert(std::abs(axis.squaredNorm() - 1) < 0.0001);

	double c = std::cos(theta);
	double s = std::sin(theta);
	double x = axis[0];
	double y = axis[1];
	double z = axis[2];

	Matrix4D mat;
	mat << c + x * x * (1 - c), x * y * (1 - c) - z * s, x * z * (1 - c) + y * s, 0,
		y * x * (1 - c) + z * s, c + y * y * (1 - c), y * z * (1 - c) - x * s, 0,
		z * x * (1 - c) - y * s, z * y * (1 - c) + x * s, c + z * z * (1 - c), 0,
		0, 0, 0, 1;

	return mat;
}

Matrix4D cMathUtil::RotateMat(const tQuaternion& q)
{
	Matrix4D mat = Matrix4D::Identity();

	double sqw = q.w() * q.w();
	double sqx = q.x()*  q.x();
	double sqy = q.y() * q.y();
	double sqz = q.z() * q.z();
	double invs = 1 / (sqx + sqy + sqz + sqw);

	mat(0, 0) = (sqx - sqy - sqz + sqw) * invs;
	mat(1, 1) = (-sqx + sqy - sqz + sqw) * invs;
	mat(2, 2) = (-sqx - sqy + sqz + sqw) * invs;

	double tmp1 = q.x()*q.y();
	double tmp2 = q.z()*q.w();
	mat(1, 0) = 2.0 * (tmp1 + tmp2) * invs;
	mat(0, 1) = 2.0 * (tmp1 - tmp2) * invs;

	tmp1 = q.x()*q.z();
	tmp2 = q.y()*q.w();
	mat(2, 0) = 2.0 * (tmp1 - tmp2) * invs;
	mat(0, 2) = 2.0 * (tmp1 + tmp2) * invs;

	tmp1 = q.y()*q.z();
	tmp2 = q.x()*q.w();
	mat(2, 1) = 2.0 * (tmp1 + tmp2) * invs;
	mat(1, 2) = 2.0 * (tmp1 - tmp2) * invs;

	return mat;
}


Matrix4D cMathUtil::InvRigidMat(const Matrix4D& mat)
{
	Matrix4D inv_mat = Matrix4D::Zero();
	inv_mat.block(0, 0, 3, 3) = mat.block(0, 0, 3, 3).transpose();
	inv_mat.col(3) = -inv_mat * mat.col(3);
	inv_mat(3, 3) = 1;
	return inv_mat;
}



void cMathUtil::EulerToAxisAngle(const Vector4D& euler, Vector4D& out_axis, double& out_theta)
{
	double x = euler[0];
	double y = euler[1];
	double z = euler[2];

	double x_s = std::sin(x);
	double x_c = std::cos(x);
	double y_s = std::sin(y);
	double y_c = std::cos(y);
	double z_s = std::sin(z);
	double z_c = std::cos(z);

	double c = (y_c * z_c + x_s * y_s * z_s + x_c * z_c + x_c * y_c - 1) * 0.5;
	//c = Clamp(c, -1.0, 1.0);
	if (c < -1.0)c = -1.0;
	if (c > 1.0)c = 1.0;

	out_theta = std::acos(c);
	if (std::abs(out_theta) < 0.00001)
	{
		out_axis = Vector4D(0, 0, 1, 0);
	}
	else
	{
		double m21 = x_s * y_c - x_c * y_s * z_s + x_s * z_c;
		double m02 = x_c * y_s * z_c + x_s * z_s + y_s;
		double m10 = y_c * z_s - x_s * y_s * z_c + x_c * z_s;
		double denom = std::sqrt(m21 * m21 + m02 * m02 + m10 * m10);
		out_axis[0] = m21 / denom;
		out_axis[1] = m02 / denom;
		out_axis[2] = m10 / denom;
		out_axis[3] = 0;
	}
}


tQuaternion cMathUtil::VecToQuat(const Vector4D& v)
{
	return tQuaternion(v[0], v[1], v[2], v[3]);
}

#endif 