#ifndef DRAW_DEEP_MIMIC_CORE_H
#define DRAW_DEEP_MIMIC_CORE_H


#include <iostream>

#include "MathUtil.h"
#include "Timer.h"
#include "DrawSceneKinChar.h"

 

class cDeepMimicCore
{
public:
	cDeepMimicCore(bool enable_draw);
	virtual ~cDeepMimicCore();
 
    virtual void Init( );
	virtual void Update(cDrawSceneKinChar* mScene, double timestep);
	 
	virtual void Draw(cDrawSceneKinChar* mScene);
	virtual void Keyboard(cDrawSceneKinChar* mScene, int key, int x, int y);
	virtual void MouseClick(cDrawSceneKinChar* mScene,int button, int state, int x, int y);
	virtual void MouseMove(cDrawSceneKinChar* mScene,int x, int y);
	virtual void Reshape(cDrawSceneKinChar* mScene, int w, int h);
	 
	virtual bool IsDone() const;
	virtual void SetUpdatesPerSec(double updates_per_sec);
	virtual int GetWinWidth() const;
	virtual int GetWinHeight() const;
	virtual int GetNumUpdateSubsteps() const;
	//========================================
	cCharacter*mChar;
   // unsigned long int mRandSeed;

	int mNumUpdateSubsteps;
	double mPlaybackSpeed;
	double mUpdatesPerSec;  
	virtual void CalcDeviceCoord(int pixel_x, int pixel_y, double& out_device_x, double& out_device_y) const;

};

#endif