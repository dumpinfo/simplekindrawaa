#ifndef MAIN_HELP_DISP_HEAD
#define MAIN_HELP_DISP_HEAD
#include <GL/glut.h>
#include <GL/freeglut.h>

void
main_reshape(int width,  int height) 
{
   /* glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, width, height, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
#define GAP  25             // gap between subwindows 
    sub_width = (width-GAP*3)/2.0;
    sub_height = (height-GAP*3)/2.0;
    
    glutSetWindow(world);
    glutPositionWindow(GAP, GAP);
    glutReshapeWindow(sub_width, sub_height);
    glutSetWindow(screen);
    glutPositionWindow(GAP+sub_width+GAP, GAP);
    glutReshapeWindow(sub_width, sub_height);
    glutSetWindow(command);
    glutPositionWindow(GAP, GAP+sub_height+GAP);
    glutReshapeWindow(sub_width+GAP+sub_width, sub_height);*/
}

void
main_display(void)
{
    glClearColor(0.8, 0.8, 0.8, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3ub(0, 0, 0);
    /*setfont("helvetica", 12);
    drawstr(GAP, GAP-5, "Fog equation");
    drawstr(GAP+sub_width+GAP, GAP-5, "Screen-space view");
    drawstr(GAP, GAP+sub_height+GAP-5, "Command manipulation window");*/
    glutSwapBuffers();
}

void
main_keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    /*case 'l':
        mode = GL_LINEAR;
        break;
    case 'e':
        mode = GL_EXP;
        break;
    case 'x':
        mode = GL_EXP2;
        break;
    case 'c':
        clear = !clear;
        break;
    case 'r':
        color[0].value = 0.7;
        color[1].value = 0.7;
        color[2].value = 0.7;
        color[3].value = 1.0;
        density.value = 1.0;
        fstart.value = 0.5;
        fend.value = 2.0;
        break;*/
    case 27:
        exit(0);
    }
    
   // redisplay_all();
}

#endif