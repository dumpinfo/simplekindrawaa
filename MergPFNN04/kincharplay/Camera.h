#ifndef GL_CAMERA_HEAD_H
#define GL_CAMERA_HEAD_H

#include "MathUtil.h"
#include <iostream>
#include <GL/glut.h>

class cCamera
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
 
	inline cCamera();
	 	
	inline cCamera(int proj, const Vector4D& pos, const Vector4D& focus, const Vector4D& up,
             double w, double h, double near_z, double far_z);
	 
	const inline Vector4D& GetUp() const;
	Vector4D inline GetViewDir() const;
	double inline GetWidth() const;
	double inline GetHeight() const;
	double inline GetAspectRatio() const;
	double inline GetNearZ() const;
	double inline GetFarZ() const;
	
	void inline Resize(double w, double h);
	void inline SetProjFocalLen(double len);

	void inline TranslateFocus(const Vector4D& focus);

	Matrix4D inline BuildViewWorldMatrix() const;
	Matrix4D inline BuildWorldViewMatrix() const;
	Matrix4D inline BuildProjMatrix() const;
 
	void inline SetupGLView() const;
	void inline SetupGLProj() const;
 
	void inline MouseClick(int button, int state, double x, double y);
	void inline MouseMove(double x, double y);
	Matrix4D inline BuildProjMatrixProj() const;

	Vector4D mPosition;
	Vector4D mFocus;
	Vector4D mUp;

	double mWidth;
	double mAspectRatio;
	double mNearZ;
	double mFarZ;

	bool mMouseDown;
	Vector4D mMousePos;
	double mProjFocalLen;

};


cCamera::cCamera()
{
	mPosition = Vector4D(0, 0, 1, 0);
	mFocus = Vector4D(0, 0, 0, 0);
	mUp = Vector4D(0, 1, 0, 0);
	mNearZ = 0;
	mFarZ = 1;

	mMouseDown = 0;
	mMousePos = Vector4D::Zero();

	Resize(1, 1);
	SetProjFocalLen((mFocus - mPosition).norm());
}



cCamera::cCamera(int proj, const Vector4D& pos, const Vector4D& focus, const Vector4D& up,
	double w, double h, double near_z, double far_z)
{
	mPosition = pos;
	mFocus = focus;
	mUp = up;
	mNearZ = near_z;
	mFarZ = far_z;
	 
	Resize(w, h);

	mMouseDown = 0;
	mMousePos = Vector4D::Zero();
	SetProjFocalLen((mFocus - mPosition).norm());
}

 

const Vector4D& cCamera::GetUp() const
{
	return mUp;
}

Vector4D cCamera::GetViewDir() const
{
	Vector4D dir = mFocus - mPosition;
	dir[3] = 0;
	dir = dir.normalized();
	return dir;
}


double cCamera::GetWidth() const
{
	return mWidth;
}
double cCamera::GetHeight() const
{
	return mWidth / mAspectRatio;
}

double cCamera::GetAspectRatio() const
{
	return mAspectRatio;
}


double cCamera::GetNearZ() const
{
	return mNearZ;
}

double cCamera::GetFarZ() const
{
	return mFarZ;
}



void cCamera::Resize(double w, double h)
{
	mWidth = w;
	mAspectRatio = w / h;
}

void cCamera::SetProjFocalLen(double len)
{
	mProjFocalLen = len;
}


void cCamera::TranslateFocus(const Vector4D& focus)
{
	Vector4D delta = focus - mFocus;
	mPosition += delta;
	mFocus = focus;
}



Matrix4D cCamera::BuildViewWorldMatrix() const
{
	Vector4D up = GetUp();
	const Vector4D& forward = GetViewDir();
	Vector4D left = up.cross3(forward).normalized();
	up = -left.cross3(forward).normalized();
	const Vector4D& pos = mPosition;

	Matrix4D T;
	T.col(0) = -left;
	T.col(1) = up;
	T.col(2) = -forward;
	T.col(3) = pos;
	T(3, 3) = 1;

	return T;
}

Matrix4D cCamera::BuildWorldViewMatrix() const
{
	Matrix4D view_world = BuildViewWorldMatrix();
	Matrix4D world_view = cMathUtil::InvRigidMat(view_world);
	return world_view;
}


Matrix4D cCamera::BuildProjMatrixProj() const
{
	Matrix4D mat = Matrix4D::Zero();
	double focal_len = mProjFocalLen;
	double w = GetWidth();
	double h = GetHeight();

	mat(0, 0) = 2 * focal_len / w;
	mat(1, 1) = 2 * focal_len / h;
	mat(2, 2) = -(mFarZ + mNearZ) / (mFarZ - mNearZ);
	mat(2, 3) = -2 * mFarZ * mNearZ / (mFarZ - mNearZ);
	mat(3, 2) = -1;

	return mat;
}

Matrix4D cCamera::BuildProjMatrix() const
{
	Vector4D cam_focus = mFocus;
	Vector4D cam_pos = mPosition;
	double w = GetWidth();
	double h = GetHeight();
	double near_z = GetNearZ();
	double far_z = GetFarZ();
	double aspect = GetAspectRatio();

	Matrix4D proj_mat;
	proj_mat = BuildProjMatrixProj();

	return proj_mat;
}



void cCamera::SetupGLView() const
{
	GLint prev_mode;
	glGetIntegerv(GL_MATRIX_MODE, &prev_mode);
	glMatrixMode(GL_MODELVIEW);

	Matrix4D world_view = BuildWorldViewMatrix();
	glLoadMatrixd(world_view.data());

	glMatrixMode(prev_mode);
}

void cCamera::SetupGLProj() const
{
	GLint prev_mode;
	glGetIntegerv(GL_MATRIX_MODE, &prev_mode);
	glMatrixMode(GL_PROJECTION);

	Matrix4D proj_mat = BuildProjMatrix();
	glLoadMatrixd(proj_mat.data());

	glMatrixMode(prev_mode);
}


void cCamera::MouseClick(int button, int state, double x, double y)
{
	mMouseDown = (button == GLUT_RIGHT_BUTTON) && (state == GLUT_DOWN);
	mMousePos[0] = x;
	mMousePos[1] = y;
}

void cCamera::MouseMove(double x, double y)
{
	if (mMouseDown)
	{
		int mouse_mod = glutGetModifiers();
		double w = GetWidth();
		double h = GetHeight();

		double dx = x - mMousePos[0];
		double dy = y - mMousePos[1];

		Vector4D cam_offset = mPosition - mFocus;
		Vector4D cam_dir = -cam_offset.normalized();
		Vector4D right = -Vector4D(0, 1, 0, 0).cross3(cam_dir).normalized();
		Vector4D up = right.cross3(cam_dir).normalized();

		cam_offset = cMathUtil::RotateMat(Vector4D(0, 1, 0, 0), -M_PI * dx) * cam_offset;
		cam_offset = cMathUtil::RotateMat(right, M_PI * dy) * cam_offset;
		mPosition = mFocus + cam_offset;

		// Remember mouse coords for next time.
		mMousePos[0] = x;
		mMousePos[1] = y;
	}
}


#endif
