#ifndef GL_CAMERA_HEAD_H
#define GL_CAMERA_HEAD_H

#include "MathUtil.h"
#include <iostream>
#include <GL/glut.h>

class cCamera
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	enum eProj
	{
		eProjPerspective,
		eProjOrtho,
		eProjMax
	};

	inline cCamera();
	inline cCamera(int proj, const Vector4D& pos, const Vector4D& focus, const Vector4D& up,
			double w, double h, double near_z, double far_z);
	virtual inline ~cCamera();

	virtual inline const Vector4D& GetPosition() const;
	virtual inline Vector4D GetFocus() const;
	virtual inline const Vector4D& GetUp() const;
	virtual inline Vector4D GetViewDir() const;
	virtual inline double GetWidth() const;
	virtual inline double GetHeight() const;
	 

	 virtual inline void SetFocus(const Vector4D& focus);
	 virtual inline void Resize(double w, double h);
	 virtual inline void TranslateFocus(const Vector4D& focus);
	  
	virtual inline Matrix4D BuildViewWorldMatrix() const;
	virtual inline Matrix4D BuildWorldViewMatrix() const;
	virtual inline Matrix4D BuildProjMatrix() const;

	 
	virtual inline void SetupGLView() const;
	virtual inline void SetupGLProj() const;

	 
	virtual inline void MouseClick(int button, int state, double x, double y);
	virtual inline void MouseMove(double x, double y);

//protected:
	eProj mProj;

	Vector4D mPosition;
	Vector4D mFocusDelta;
	Vector4D mUp;

	double mWidth;
	double mAspectRatio;
	double mNearZ;
	double mFarZ;

	bool mMouseDown;
	Vector4D mMousePos;

	virtual inline double CalcFocalLen() const;
	virtual inline Matrix4D BuildProjMatrixProj() const;

	virtual inline void Zoom(double zoom);
};  

 

cCamera::cCamera()
{
	mPosition = Vector4D(0, 0, 1, 0);
	mFocusDelta = -mPosition;
	mUp = Vector4D(0, 1, 0, 0);
	mNearZ = 0;
	mFarZ = 1;

	mProj = eProjPerspective;

	mMouseDown = 0;
	mMousePos = Vector4D::Zero();

	Resize(1, 1);
}

cCamera::~cCamera()
{
}

cCamera::cCamera(int proj, const Vector4D& pos, const Vector4D& focus,
	const Vector4D& up, double w, double h, double near_z, double far_z)
{
	mPosition = pos;
	mFocusDelta = focus - pos;
	mUp = up;
	mNearZ = near_z;
	mFarZ = far_z;
	//mProj = proj;

	Resize(w, h);

	mMouseDown = 0;
	mMousePos = Vector4D::Zero();
}

const Vector4D& cCamera::GetPosition() const
{
	return mPosition;
}

Vector4D cCamera::GetFocus() const
{
	return mFocusDelta + mPosition;
}

const Vector4D& cCamera::GetUp() const
{
	return mUp;
}

Vector4D cCamera::GetViewDir() const
{
	Vector4D dir = mFocusDelta;
	dir[3] = 0;
	dir = dir.normalized();
	return dir;
}


double cCamera::GetWidth() const
{
	return mWidth;
}
double cCamera::GetHeight() const
{
	return mWidth / mAspectRatio;
}



void cCamera::SetFocus(const Vector4D& focus)
{
	mFocusDelta = focus - mPosition;
}



void cCamera::Resize(double w, double h)
{
	mWidth = w;
	mAspectRatio = w / h;
}



void cCamera::TranslateFocus(const Vector4D& focus)
{
	mPosition = focus - mFocusDelta;
}


Matrix4D cCamera::BuildViewWorldMatrix() const
{
	Vector4D up = GetUp();
	const Vector4D& forward = GetViewDir();
	Vector4D left = up.cross3(forward).normalized();
	up = -left.cross3(forward).normalized();
	const Vector4D& pos = GetPosition();

	Matrix4D T;
	T.col(0) = -left;
	T.col(1) = up;
	T.col(2) = -forward;
	T.col(3) = pos;
	T(3, 3) = 1;

	return T;
}

Matrix4D cCamera::BuildWorldViewMatrix() const
{
	Matrix4D view_world = BuildViewWorldMatrix();
	Matrix4D world_view = cMathUtil::InvRigidMat(view_world);
	return world_view;
}

Matrix4D cCamera::BuildProjMatrix() const
{
	Matrix4D proj_mat;
	 
    proj_mat = BuildProjMatrixProj();
	 
	return proj_mat;
}



void cCamera::SetupGLView() const
{
	GLint prev_mode;
	glGetIntegerv(GL_MATRIX_MODE, &prev_mode);
	glMatrixMode(GL_MODELVIEW);

	Matrix4D world_view = BuildWorldViewMatrix();
	glLoadMatrixd(world_view.data());

	glMatrixMode(prev_mode);
}

void cCamera::SetupGLProj() const
{
	GLint prev_mode;
	glGetIntegerv(GL_MATRIX_MODE, &prev_mode);
	glMatrixMode(GL_PROJECTION);

	Matrix4D proj_mat = BuildProjMatrix();
	glLoadMatrixd(proj_mat.data());

	glMatrixMode(prev_mode);
}



void cCamera::MouseClick(int button, int state, double x, double y)
{
	mMouseDown = (button == GLUT_RIGHT_BUTTON) && (state == GLUT_DOWN);
	mMousePos[0] = x;
	mMousePos[1] = y;

	bool mouse_wheel = (button == 3) || (button == 4);
	if (mouse_wheel)
	{
		double zoom = (button == 3) ? -0.05 : 0.05;
		Zoom(zoom);
	}
}

void cCamera::MouseMove(double x, double y)
{
	if (mMouseDown)
	{
		int mouse_mod = glutGetModifiers();
		double w = GetWidth();
		double h = GetHeight();

		double dx = x - mMousePos[0];
		double dy = y - mMousePos[1];

		Vector4D focus = GetFocus();
		Vector4D cam_offset = -mFocusDelta;
		Vector4D cam_dir = -cam_offset.normalized();
		Vector4D right = -mUp.cross3(cam_dir).normalized();
		mUp = right.cross3(cam_dir).normalized();

		/*if (mouse_mod & GLUT_ACTIVE_ALT) {
			// translate
			Vector4D delta = 0.5 * (-w * right * dx - h * mUp * dy);
			mPosition += delta;
			focus += delta;
		}
		else*/
		{
			// rotate
			Matrix4D rot_mat = cMathUtil::RotateMat(Vector4D(0, 1, 0, 0), -M_PI * dx)
				* cMathUtil::RotateMat(right, M_PI * dy);
			cam_offset = rot_mat * cam_offset;
			mUp = rot_mat * mUp;
			mPosition = focus + cam_offset;
		}

		SetFocus(focus);

		// Remember mouse coords for next time.
		mMousePos[0] = x;
		mMousePos[1] = y;
	}
}

double cCamera::CalcFocalLen() const
{
	return mFocusDelta.norm();
}


Matrix4D cCamera::BuildProjMatrixProj() const
{
	Matrix4D mat = Matrix4D::Zero();
	double focal_len = CalcFocalLen();//mProjFocalLen;
	double w = GetWidth();
	double h = GetHeight();

	mat(0, 0) = 2 * focal_len / w;
	mat(1, 1) = 2 * focal_len / h;
	mat(2, 2) = -(mFarZ + mNearZ) / (mFarZ - mNearZ);
	mat(2, 3) = -2 * mFarZ * mNearZ / (mFarZ - mNearZ);
	mat(3, 2) = -1;

	return mat;
}


void cCamera::Zoom(double zoom)
{
	Vector4D focus = GetFocus();
	Vector4D cam_offset = -mFocusDelta;
	double w = GetWidth();
	double h = GetHeight();

	double delta_scale = 1 - zoom;
	Vector4D delta = cam_offset * delta_scale;
	mPosition = focus + delta;
	w *= delta_scale;
	h *= delta_scale;
	Resize(w, h);
	SetFocus(focus);
}

#endif
