#ifndef BODY_KINTREE_HEAD_H
#define BODY_KINTREE_HEAD_H

#include <vector>
#include <fstream>
#include <iostream>
//#include "Shape.h"
#include "json/json.h"
#include "MathUtil.h"


class cShape
{
public:
	enum eShape
	{
		eShapeNull,
		eShapeBox,
		eShapeCapsule,
		eShapeSphere,
		eShapeCylinder,
		eShapePlane,
		eShapeMax,
	};

	static bool inline ParseShape(const std::string& str, cShape::eShape& out_shape);
};

bool  inline cShape::ParseShape(const std::string& str, eShape& out_shape)
{
	bool succ = true;
	if (str == "null")
	{
		out_shape = eShapeNull;
	}
	else if (str == "box")
	{
		out_shape = eShapeBox;
	}
	else if (str == "capsule")
	{
		out_shape = eShapeCapsule;
	}
	else if (str == "sphere")
	{
		out_shape = eShapeSphere;
	}
	else if (str == "cylinder")
	{
		out_shape = eShapeCylinder;
	}
	else if (str == "plane")
	{
		out_shape = eShapePlane;
	}
	else
	{
		printf("Unsupported body shape %s\n", str.c_str());
		assert(false);
	}
	return succ;
}
//---------------------------------------------------------------
class cKinTree
{
public:
	// description of the joint tree representing an articulated figure
	enum eJointType
	{
		eJointTypeRevolute,
		eJointTypePlanar,
		eJointTypePrismatic,
		eJointTypeFixed,
		eJointTypeSpherical,
		eJointTypeNone,
		eJointTypeMax
	};

	enum eJointDesc
	{
		eJointDescType,
		eJointDescParent,
		eJointDescAttachX,
		eJointDescAttachY,
		eJointDescAttachZ,
		eJointDescAttachThetaX, // euler angles order rot(Z) * rot(Y) * rot(X)
		eJointDescAttachThetaY,
		eJointDescAttachThetaZ,
		eJointDescLimLow0,
		eJointDescLimLow1,
		eJointDescLimLow2,
		eJointDescLimHigh0,
		eJointDescLimHigh1,
		eJointDescLimHigh2,
		eJointDescTorqueLim,
		eJointDescForceLim,
		eJointDescIsEndEffector,
		eJointDescDiffWeight,
		eJointDescParamOffset,
		eJointDescMax
	};
	typedef Eigen::Matrix<double, 1, eJointDescMax> tJointDesc;

	enum eBodyParam
	{
		eBodyParamShape,
		eBodyParamMass,
		eBodyParamColGroup, // 0 collides with nothing and 1 collides with everything
		eBodyParamEnableFallContact,
		eBodyParamAttachX,
		eBodyParamAttachY,
		eBodyParamAttachZ,
		eBodyParamAttachThetaX, // Euler angles order XYZ
		eBodyParamAttachThetaY,
		eBodyParamAttachThetaZ,
		eBodyParam0,
		eBodyParam1,
		eBodyParam2,
		eBodyColorR,
		eBodyColorG,
		eBodyColorB,
		eBodyColorA,
		eBodyParamMax
	};
	typedef Eigen::Matrix<double, 1, eBodyParamMax> tBodyDef;

	enum eDrawShape
	{
		eDrawShapeShape,
		eDrawShapeParentJoint,
		eDrawShapeAttachX,
		eDrawShapeAttachY,
		eDrawShapeAttachZ,
		eDrawShapeAttachThetaX, // Euler angles order XYZ
		eDrawShapeAttachThetaY,
		eDrawShapeAttachThetaZ,
		eDrawShapeParam0,
		eDrawShapeParam1,
		eDrawShapeParam2,
		eDrawShapeColorR,
		eDrawShapeColorG,
		eDrawShapeColorB,
		eDrawShapeColorA,
		eDrawShapeMeshID,
		eDrawShapeParamMax
	};
	typedef Eigen::Matrix<double, 1, eDrawShapeParamMax> tDrawShapeDef;

	 
	static Vector4D inline GetRootPos(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state);
	static void inline SetRootPos(const Eigen::MatrixXd& joint_mat, const Vector4D& pos, Eigen::VectorXd& out_state);
	static tQuaternion inline GetRootRot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state);
	 
 	static int inline GetParamOffset(const Eigen::MatrixXd& joint_mat, int joint_id);
	static int inline GetParamSize(const Eigen::MatrixXd& joint_mat, int joint_id);
	static int inline GetJointParamSize(eJointType joint_type);
	static eJointType inline GetJointType(const Eigen::MatrixXd& joint_mat, int joint_id);
	static int inline GetParent(const Eigen::MatrixXd& joint_mat, int joint_id);
	static bool inline HasParent(const Eigen::MatrixXd& joint_mat, int joint_id);
	static bool inline IsRoot(const Eigen::MatrixXd& joint_mat, int joint_id);
	static Vector4D inline GetAttachPt(const Eigen::MatrixXd& joint_mat, int joint_id);
	static Vector4D inline GetAttachTheta(const Eigen::MatrixXd& joint_mat, int joint_id);
    static Matrix4D inline BuildAttachTrans(const Eigen::MatrixXd& joint_mat, int joint_id);
	static Matrix4D inline ChildParentTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	static Matrix4D inline JointWorldTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	 
	static bool inline Load(const Json::Value& root, Eigen::MatrixXd& out_joint_mat);
	static int inline GetNumJoints(const Eigen::MatrixXd& joint_mat);
	static int inline GetRoot(const Eigen::MatrixXd& joint_mat);
	 
	static bool inline LoadDrawShapeDefs(const std::string& char_file, Eigen::MatrixXd& out_draw_defs);
	static bool inline ParseDrawShapeDef(const Json::Value& root, tDrawShapeDef& out_def);
	
	static int inline GetDrawShapeParentJoint(const tDrawShapeDef& shape);
	static Vector4D inline GetDrawShapeAttachPt(const tDrawShapeDef& shape);
	static Vector4D inline GetDrawShapeAttachTheta(const tDrawShapeDef& shape);
	static void inline GetDrawShapeRotation(const tDrawShapeDef& shape, Vector4D& out_axis, double& out_theta);
	static Vector4D inline GetDrawShapeColor(const tDrawShapeDef& shape);
	static tJointDesc inline BuildJointDesc();
	static tDrawShapeDef inline BuildDrawShapeDef();
 
	static void inline PostProcessPose(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose);
	public:
	static bool inline ParseJoint(const Json::Value& root, tJointDesc& out_joint_desc);
	static bool inline ParseJointType(const std::string& type_str, eJointType& out_joint_type);
	static void inline PostProcessJointMat(Eigen::MatrixXd& out_joint_mat);

	static Matrix4D inline ChildParentTransRoot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	static Matrix4D inline ChildParentTransRevolute(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	static Matrix4D inline ChildParentTransFixed(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	static Matrix4D inline ChildParentTransSpherical(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id);
	static void inline FindChildren(const Eigen::MatrixXd& joint_desc, int joint_id, Eigen::VectorXi& out_children);
};


void cKinTree::FindChildren(const Eigen::MatrixXd& joint_desc, int joint_id, Eigen::VectorXi& out_children)
{
	const int max_size = 128;
	int children_buffer[max_size];
	int num_children = 0;
	int num_joints = GetNumJoints(joint_desc);

	for (int i = 0; i < num_joints; ++i)
	{
		int parent = GetParent(joint_desc, i);
		if (parent == joint_id)
		{
			children_buffer[num_children] = i;
			++num_children;

			if (num_children >= max_size)
			{
				printf("Too many children, max = %i", max_size);
				assert(false);
				return;
			}
		}
	}

	out_children.resize(num_children);
	for (int i = 0; i < num_children; ++i)
	{
		out_children[i] = children_buffer[i];
	}
}


int cKinTree::GetRoot(const Eigen::MatrixXd& joint_desc)
{
	// this should always be true right?
	return 0;
}



bool cKinTree::LoadDrawShapeDefs(const std::string& char_file, Eigen::MatrixXd& out_draw_defs)
{
	bool succ = true;
	std::string str;

	std::ifstream f_stream(char_file.c_str());
	Json::Value root;
	Json::Reader reader;
	succ = reader.parse(f_stream, root);
	f_stream.close();

	if (succ)
	{
		if (!root["DrawShapeDefs"].isNull())
		{
			Json::Value shape_defs = root.get("DrawShapeDefs", 0);
			int num_shapes = shape_defs.size();

			succ = true;
			out_draw_defs.resize(num_shapes, eDrawShapeParamMax);
			for (int b = 0; b < num_shapes; ++b)
			{
				tDrawShapeDef curr_def = BuildDrawShapeDef();
				Json::Value shape_json = shape_defs.get(b, 0);
				bool succ_def = ParseDrawShapeDef(shape_json, curr_def);

				if (succ)
				{
					out_draw_defs.row(b) = curr_def;
				}

			}
		}
	}



	return succ;
}

bool cKinTree::ParseDrawShapeDef(const Json::Value& root, tDrawShapeDef& out_def)
{
	const std::string gDrawShapeDescKeys[cKinTree::eDrawShapeParamMax] =
	{
		"Shape",
		"ParentJoint",
		"AttachX",
		"AttachY",
		"AttachZ",
		"AttachThetaX",
		"AttachThetaY",
		"AttachThetaZ",
		"Param0",
		"Param1",
		"Param2",
		"ColorR",
		"ColorG",
		"ColorB",
		"ColorA",
		"MeshID"
	};
	std::string shape_str = root.get(gDrawShapeDescKeys[eDrawShapeShape], "").asString();
	cShape::eShape shape;
	bool succ = cShape::ParseShape(shape_str, shape);
	if (succ)
	{
		out_def(eDrawShapeShape) = static_cast<double>(static_cast<int>(shape));
	}

	for (int i = 0; i < eDrawShapeParamMax; ++i)
	{
		const std::string& curr_key = gDrawShapeDescKeys[i];
		if (!root[curr_key].isNull()
			&& root[curr_key].isNumeric())
		{
			Json::Value json_val = root[curr_key];
			double val = json_val.asDouble();
			out_def(i) = val;
		}
	}

	return succ;
}



int cKinTree::GetDrawShapeParentJoint(const tDrawShapeDef& shape)
{
	return static_cast<int>(shape[eDrawShapeParentJoint]);
}

Vector4D cKinTree::GetDrawShapeAttachPt(const tDrawShapeDef& shape)
{
	return Vector4D(shape[eDrawShapeAttachX], shape[eDrawShapeAttachY], shape[cKinTree::eDrawShapeAttachZ], 0);
}

Vector4D cKinTree::GetDrawShapeAttachTheta(const tDrawShapeDef& shape)
{
	return Vector4D(shape[eDrawShapeAttachThetaX], shape[eDrawShapeAttachThetaY], shape[eDrawShapeAttachThetaZ], 0);
}

void cKinTree::GetDrawShapeRotation(const tDrawShapeDef& shape, Vector4D& out_axis, double& out_theta)
{
	Vector4D theta = GetDrawShapeAttachTheta(shape);
	cMathUtil::EulerToAxisAngle(theta, out_axis, out_theta);
}

Vector4D cKinTree::GetDrawShapeColor(const tDrawShapeDef& shape)
{
	return Vector4D(shape[eDrawShapeColorR], shape[eDrawShapeColorG], shape[eDrawShapeColorB], shape[cKinTree::eDrawShapeColorA]);
}



bool cKinTree::Load(const Json::Value& root, Eigen::MatrixXd& out_joint_mat)
{
	bool succ = false;

	if (!root["Joints"].isNull())
	{
		Json::Value joints = root["Joints"];
		int num_joints = joints.size();

		out_joint_mat.resize(num_joints, eJointDescMax);

		for (int j = 0; j < num_joints; ++j)
		{
			tJointDesc curr_joint_desc = tJointDesc::Zero();

			Json::Value joint_json = joints.get(j, 0);
			succ = ParseJoint(joint_json, curr_joint_desc);
			if (succ)
			{
				out_joint_mat.row(j) = curr_joint_desc;
			}

		}

		for (int j = 0; j < num_joints; ++j)
		{
			const auto& curr_desc = out_joint_mat.row(j);
			int parent_id = static_cast<int>(curr_desc(eJointDescParent));

			out_joint_mat.row(j) = curr_desc;
		}

		PostProcessJointMat(out_joint_mat);
	}

	return succ;
}



Vector4D cKinTree::GetRootPos(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state)
{
	const int  gPosDim = 3;
	int root_id = GetRoot(joint_mat);
	Vector4D pos = Vector4D::Zero();
	int param_offset = GetParamOffset(joint_mat, root_id);
	pos.segment(0, gPosDim) = state.segment(param_offset, gPosDim);
	return pos;
}

void cKinTree::SetRootPos(const Eigen::MatrixXd& joint_mat, const Vector4D& pos, Eigen::VectorXd& out_state)
{
	const int  gPosDim = 3;
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	out_state.segment(param_offset, gPosDim) = pos.segment(0, gPosDim);
}

tQuaternion cKinTree::GetRootRot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state)
{
	const int  gPosDim = 3;
	const int  gRotDim = 4;
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	tQuaternion rot = cMathUtil::VecToQuat(state.segment(param_offset + gPosDim, gRotDim));
	return rot;
}

 
int cKinTree::GetNumJoints(const Eigen::MatrixXd& joint_mat)
{
	return static_cast<int>(joint_mat.rows());
}

 
int cKinTree::GetParamOffset(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	int offset = static_cast<int>(joint_mat(joint_id, eJointDescParamOffset));
	return offset;
}

int cKinTree::GetParamSize(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	const int  gRootDim = 7;
	eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	bool is_root = cKinTree::IsRoot(joint_mat, joint_id);
	int size = (is_root) ? gRootDim : GetJointParamSize(joint_type);
	return size;
}

int cKinTree::GetJointParamSize(eJointType joint_type)
{
	int size = 0;
	switch (joint_type)
	{
	case eJointTypeRevolute:
		size = 1;
		break;
	case eJointTypePrismatic:
		size = 1;
		break;
	case eJointTypePlanar:
		size = 3;
		break;
	case eJointTypeFixed:
		size = 0;
		break;
	case eJointTypeSpherical:
		size = 4;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}

	return size;
}

 
cKinTree::eJointType cKinTree::GetJointType(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	eJointType type = static_cast<eJointType>(static_cast<int>(joint_mat(joint_id, cKinTree::eJointDescType)));
	return type;
}

int cKinTree::GetParent(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	int parent = static_cast<int>(joint_mat(joint_id, cKinTree::eJointDescParent));
	assert(parent < joint_id); // joints should always be ordered as such
								// since some algorithms will assume this ordering
	return parent;
}

bool cKinTree::HasParent(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	const int  gInvalidJointID = -1;
	int parent = GetParent(joint_mat, joint_id);
	return parent != gInvalidJointID;
}

bool cKinTree::IsRoot(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	return !HasParent(joint_mat, joint_id);
}



Vector4D cKinTree::GetAttachPt(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	Vector4D attach_pt = Vector4D(joint_mat(joint_id, cKinTree::eJointDescAttachX),
		joint_mat(joint_id, cKinTree::eJointDescAttachY),
		joint_mat(joint_id, cKinTree::eJointDescAttachZ), 0);
	return attach_pt;
}

Vector4D cKinTree::GetAttachTheta(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	Vector4D attach_theta = Vector4D(joint_mat(joint_id, cKinTree::eJointDescAttachThetaX),
		joint_mat(joint_id, cKinTree::eJointDescAttachThetaY),
		joint_mat(joint_id, cKinTree::eJointDescAttachThetaZ), 0);
	return attach_theta;
}



bool cKinTree::ParseJoint(const Json::Value& root, tJointDesc& out_joint_desc)
{
	const std::string gJointDescKeys[cKinTree::eJointDescMax] =
	{
		"Type",
		"Parent",
		"AttachX",
		"AttachY",
		"AttachZ",
		"AttachThetaX",
		"AttachThetaY",
		"AttachThetaZ",
		"LimLow0",
		"LimLow1",
		"LimLow2",
		"LimHigh0",
		"LimHigh1",
		"LimHigh2",
		"TorqueLim",
		"ForceLim",
		"IsEndEffector",
		"DiffWeight",
		"Offset"
	};
	out_joint_desc = BuildJointDesc();
	eJointType joint_type = eJointTypeNone;
	const Json::Value& type_json = root[gJointDescKeys[eJointDescType]];
	if (type_json.isNull())
	{
		printf("No joint type specified\n");
	}
	else
	{
		std::string type_str = type_json.asString();
		ParseJointType(type_str, joint_type);
		out_joint_desc[eJointDescType] = static_cast<double>(static_cast<int>(joint_type));
	}

	for (int i = 0; i < eJointDescMax; ++i)
	{
		if (i != eJointDescType)
		{
			const std::string& key = gJointDescKeys[i];
			if (!root[key].isNull())
			{
				out_joint_desc[i] = root[key].asDouble();
			}
		}
	}
	return true;
}

bool cKinTree::ParseJointType(const std::string& type_str, eJointType& out_joint_type)
{
	// Json keys
	const std::string gJointTypeNames[cKinTree::eJointTypeMax] =
	{
		"revolute",
		"planar",
		"prismatic",
		"fixed",
		"spherical",
		"none"
	};

	for (int i = 0; i < eJointTypeMax; ++i)
	{
		const std::string& name = gJointTypeNames[i];
		if (type_str == name)
		{
			out_joint_type = static_cast<eJointType>(i);
			return true;
		}
	}
	printf("Unsupported joint type: %s\n", type_str.c_str());
	assert(false); // unsupported joint type
	return false;
}

void cKinTree::PostProcessJointMat(Eigen::MatrixXd& out_joint_mat)
{
	int num_joints = GetNumJoints(out_joint_mat);
	int offset = 0;
	for (int j = 0; j < num_joints; ++j)
	{
		int curr_size = GetParamSize(out_joint_mat, j);
		out_joint_mat(j, eJointDescParamOffset) = offset;
		offset += curr_size;
	}
	int root_id = GetRoot(out_joint_mat);

	out_joint_mat(root_id, eJointDescAttachX) = 0;
	out_joint_mat(root_id, eJointDescAttachY) = 0;
	out_joint_mat(root_id, eJointDescAttachZ) = 0;
}

Matrix4D cKinTree::BuildAttachTrans(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	// child to parent
	Vector4D attach_pt = GetAttachPt(joint_mat, joint_id);
	Vector4D attach_theta = GetAttachTheta(joint_mat, joint_id);
	Matrix4D mat = cMathUtil::RotateMat(attach_theta);
	mat(0, 3) = attach_pt[0];
	mat(1, 3) = attach_pt[1];
	mat(2, 3) = attach_pt[2];
	return mat;
}

Matrix4D cKinTree::ChildParentTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	Matrix4D mat;
	eJointType j_type = GetJointType(joint_mat, joint_id);
	bool is_root = IsRoot(joint_mat, joint_id);

	if (is_root)
	{
		mat = ChildParentTransRoot(joint_mat, state, joint_id);
	}
	else
	{
		switch (j_type)
		{
		case eJointTypeRevolute:
			mat = ChildParentTransRevolute(joint_mat, state, joint_id);
			break;
		case eJointTypePrismatic:
			//<--- mat = ChildParentTransPrismatic(joint_mat, state, joint_id);
			break;
		case eJointTypePlanar:
			//<--- mat = ChildParentTransPlanar(joint_mat, state, joint_id);
			break;
		case eJointTypeFixed:
			mat = ChildParentTransFixed(joint_mat, state, joint_id);
			break;
		case eJointTypeSpherical:
			mat = ChildParentTransSpherical(joint_mat, state, joint_id);
			break;
		default:
			break;
		}
	}

	return mat;
}



Matrix4D cKinTree::JointWorldTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	const int  gInvalidJointID = -1;
	Matrix4D m = Matrix4D::Identity();
	int curr_id = joint_id;
	assert(joint_id != gInvalidIdx); // invalid joint
	while (curr_id != gInvalidJointID)
	{
		Matrix4D child_parent_mat = ChildParentTrans(joint_mat, state, curr_id);
		m = child_parent_mat * m;
		curr_id = GetParent(joint_mat, curr_id);
	}

	return m;
}



cKinTree::tJointDesc cKinTree::BuildJointDesc()
{
	tJointDesc desc;
	desc(eJointDescType) = static_cast<double>(eJointTypeRevolute);
	desc(eJointDescParent) = gInvalidIdx;
	desc(eJointDescAttachX) = 0;
	desc(eJointDescAttachY) = 0;
	desc(eJointDescAttachZ) = 0;
	desc(eJointDescAttachThetaX) = 0;
	desc(eJointDescAttachThetaY) = 0;
	desc(eJointDescAttachThetaZ) = 0;
	desc(eJointDescLimLow0) = 1;
	desc(eJointDescLimLow1) = 1;
	desc(eJointDescLimLow2) = 1;
	desc(eJointDescLimHigh0) = 0;
	desc(eJointDescLimHigh1) = 0;
	desc(eJointDescLimHigh2) = 0;
	desc(eJointDescIsEndEffector) = 0;
	desc(eJointDescTorqueLim) = std::numeric_limits<double>::infinity();
	desc(eJointDescForceLim) = std::numeric_limits<double>::infinity();
	desc(eJointDescDiffWeight) = 1;
	desc(eJointDescParamOffset) = 0;

	return desc;
}



cKinTree::tDrawShapeDef cKinTree::BuildDrawShapeDef()
{
	tDrawShapeDef def;
	def(eDrawShapeShape) = static_cast<double>(cShape::eShapeNull);
	def(eDrawShapeParentJoint) = gInvalidIdx;
	def(eDrawShapeAttachX) = 0;
	def(eDrawShapeAttachY) = 0;
	def(eDrawShapeAttachZ) = 0;
	def(eDrawShapeAttachThetaX) = 0;
	def(eDrawShapeAttachThetaY) = 0;
	def(eDrawShapeAttachThetaZ) = 0;
	def(eDrawShapeParam0) = 0;
	def(eDrawShapeParam1) = 0;
	def(eDrawShapeParam2) = 0;
	def(eDrawShapeColorR) = 0;
	def(eDrawShapeColorG) = 0;
	def(eDrawShapeColorB) = 0;
	def(eDrawShapeColorA) = 1;
	def(eDrawShapeMeshID) = gInvalidIdx;
	return def;
}



void cKinTree::PostProcessPose(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose)
{
	// mostly to normalize quaternions
	const int  gRotDim = 4; const int  gPosDim = 3;
	int num_joints = GetNumJoints(joint_mat);
	int root_id = GetRoot(joint_mat);
	int root_offset = GetParamOffset(joint_mat, root_id);
	out_pose.segment(root_offset + gPosDim, gRotDim).normalize();

	for (int j = 1; j < num_joints; ++j)
	{
		eJointType joint_type = GetJointType(joint_mat, j);
		if (joint_type == eJointTypeSpherical)
		{
			int offset = GetParamOffset(joint_mat, j);
			out_pose.segment(offset, gRotDim).normalize();
		}
	}
}
 

Matrix4D cKinTree::ChildParentTransRoot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	Vector4D offset = GetRootPos(joint_mat, state);
	tQuaternion rot = GetRootRot(joint_mat, state);

	Matrix4D A = BuildAttachTrans(joint_mat, joint_id);
	Matrix4D R = cMathUtil::RotateMat(rot);
	Matrix4D T = cMathUtil::TranslateMat(offset);

	Matrix4D mat = A * T * R;
	return mat;
}

Matrix4D cKinTree::ChildParentTransRevolute(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	int param_offset = cKinTree::GetParamOffset(joint_mat, joint_id);
	double theta = state(param_offset);

	Matrix4D A = BuildAttachTrans(joint_mat, joint_id);
	Matrix4D R = cMathUtil::RotateMat(Vector4D(0, 0, 1, 0), theta);

	Matrix4D mat = A * R;
	return mat;
}



Matrix4D cKinTree::ChildParentTransFixed(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	Matrix4D A = BuildAttachTrans(joint_mat, joint_id);
	Matrix4D mat = A;
	return mat;
}


Matrix4D cKinTree::ChildParentTransSpherical(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	int param_offset = cKinTree::GetParamOffset(joint_mat, joint_id);
	int param_size = cKinTree::GetParamSize(joint_mat, joint_id);
	tQuaternion q = cMathUtil::VecToQuat(state.segment(param_offset, param_size));

	Matrix4D A = BuildAttachTrans(joint_mat, joint_id);
	Matrix4D R = cMathUtil::RotateMat(q);

	Matrix4D mat = A * R;
	return mat;
}


#endif