#include "DeepMimicCore.h"

#include "DrawUtil.h"
#include "DrawSceneKinChar.h"

cDeepMimicCore::cDeepMimicCore(bool enable_draw)
{
	mNumUpdateSubsteps = 1;
	mPlaybackSpeed = 1;
	mUpdatesPerSec = 0;
	mChar = new cCharacter();
}

cDeepMimicCore::~cDeepMimicCore()
{
	delete mChar;
}


void cDeepMimicCore::Init()
{ 
	mChar->Init("humanoid3d.txt"); 
	cDrawUtil::InitDrawUtil();
	std::string scene_name = "";
	printf("Loaded scene KinCharPlay\n");
}

void cDeepMimicCore::Update(cDrawSceneKinChar* mScene,double timestep)
{
	 mScene->Update(timestep);
}


void cDeepMimicCore::Draw(cDrawSceneKinChar* mScene)
{
	 mScene->Draw();
}

void cDeepMimicCore::Keyboard(cDrawSceneKinChar* mScene,int key, int x, int y)
{
	char c = static_cast<char>(key);
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
}

void cDeepMimicCore::MouseClick(cDrawSceneKinChar* mScene, int button, int state, int x, int y)
{
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
	mScene->MouseClick(button, state, device_x, device_y);
}

void cDeepMimicCore::MouseMove(cDrawSceneKinChar* mScene, int x, int y)
{
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
	mScene->MouseMove(device_x, device_y);
}

void cDeepMimicCore::Reshape(cDrawSceneKinChar* mScene, int w, int h)
{
	glViewport(0, 0, w, h);
	glutPostRedisplay();
}


bool cDeepMimicCore::IsDone() const
{
	 return false;  
}

 
void cDeepMimicCore::SetUpdatesPerSec(double updates_per_sec)
{
	mUpdatesPerSec = updates_per_sec;
}
 
int cDeepMimicCore::GetWinWidth() const
{
	return 900; 
}

int cDeepMimicCore::GetWinHeight() const
{
	return 600; 
}

int cDeepMimicCore::GetNumUpdateSubsteps() const
{
	return mNumUpdateSubsteps;
}

 
 
 


void cDeepMimicCore::CalcDeviceCoord(int pixel_x, int pixel_y, double& out_device_x, double& out_device_y) const
{
	double w = GetWinWidth();
	double h = GetWinHeight();

	out_device_x = static_cast<double>(pixel_x) / w;
	out_device_y = static_cast<double>(pixel_y) / h;
	out_device_x = (out_device_x - 0.5f) * 2.f;
	out_device_y = (out_device_y - 0.5f) * -2.f;
}

 
