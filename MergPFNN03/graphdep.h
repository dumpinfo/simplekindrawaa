#ifndef LT_GRAPH_HEAD
#define LT_GRAPH_HEAD
#include "Character.h"

struct LightDirectional {
  
  glm::vec3 target;
  glm::vec3 position;
  
  GLuint fbo;
  GLuint buf;
  GLuint tex;
  
  LightDirectional()
    : target(glm::vec3(0))
    , position(glm::vec3(3000, 3700, 1500))
    , fbo(0)
    , buf(0)
    , tex(0) {
      
    //glGenFramebuffers(1, &fbo);
    //glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    //glDrawBuffer(GL_NONE);
    //glReadBuffer(GL_NONE);
    
    //glGenRenderbuffers(1, &buf);
    //glBindRenderbuffer(GL_RENDERBUFFER, buf);
    //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 1024);
    //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, buf);  
    
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
 
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    //glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex, 0);

    //_ glBindTexture(GL_TEXTURE_2D, 0);
    //_ glBindRenderbuffer(GL_RENDERBUFFER, 0);
    //_ glBindFramebuffer(GL_FRAMEBUFFER, 0);
      
  }
  
  ~LightDirectional() {
    //_ glDeleteBuffers(1, &fbo);
    //_ glDeleteBuffers(1, &buf);
    glDeleteTextures(1, &tex);
  }
  
};

int parent_arr[17][2] =
{ 0 , -1 ,
1 , 0  ,
2 , 1  ,
3 ,1   ,
4 , 3  ,
5 , 4  ,
6 ,1   ,
7 , 6  ,
8 , 7  ,
9 , 0  ,
10 , 9  ,
11 , 10 ,
12 , 11 ,
13 , 0  ,
14 , 13 ,
15 , 14 ,
16 , 15 };

Matrix4D CVTM4(const glm::mat4&mat)
{
	Matrix4D matret;
	int i, j;
	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++)
		{
			matret(i, j) = mat[j][i];
		}
	return matret;
}

glm::mat4 CVTM4(const Matrix4D&mat)
{
	glm::mat4 matret;
	int i, j;
	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++)
		{
			matret[i][j] = mat(j, i);
		}
	return matret;
}

struct Character {
  cCharacter gCharModel;
  enum { JOINT_NUM = 17 };
  
  GLuint vbo, tbo;
  int ntri, nvtx;
  float phase;
  float strafe_amount;
  float strafe_target;
  float crouched_amount;
  float crouched_target;
  float responsive;
  
  glm::vec3 joint_positions[JOINT_NUM];
  glm::vec3 joint_velocities[JOINT_NUM];
  glm::mat3 joint_rotations[JOINT_NUM];
  
  glm::mat4 joint_anim_xform[JOINT_NUM];
  glm::mat4 joint_rest_xform[JOINT_NUM];
  glm::mat4 joint_mesh_xform[JOINT_NUM];
  glm::mat4 joint_global_rest_xform[JOINT_NUM];
  glm::mat4 joint_global_anim_xform[JOINT_NUM];

  Matrix4D joint_local_transform[JOINT_NUM];

  Vector4d joint_AttachOffset[JOINT_NUM];
  //Vector4d joint_AttachOffset[JOINT_NUM];

  int joint_parents[JOINT_NUM];
  
  
  enum {
    JOINT_ROOT_L = 1,
    JOINT_HIP_L  = 2,
    JOINT_KNEE_L = 3,  
    JOINT_HEEL_L = 4,
    JOINT_TOE_L  = 5,  
      
    JOINT_ROOT_R = 6,  
    JOINT_HIP_R  = 7,  
    JOINT_KNEE_R = 8,  
    JOINT_HEEL_R = 9,
    JOINT_TOE_R  = 10  
  };

  int convert15Idto17Id(int id)
  {
	  int cvt_tb[17][2] =
	  { 0	  , 0 , // root	            
		1	  , 1 , // chest	            
		2	  , 2 , // neck	 
		9	  , 3 , // right_hip     
	   10	  , 4 , // right_knee	      
	   11	  , 5 , // right_ankle          
		3	  , 6 , // right_shoulder     
		4	  , 7 , // right_elbow	      
		5	  , 8 , // right_wrist	
	   13	  , 9 , // left_hip	 
		14	  ,10 , // left_knee	   
		15	  ,11 , // left_ankle	                
		6	  ,12 , // left_shoulder	    
		7	  ,13 , // left_elbow	        
		8	  ,14 , // left_wrist	            
	   12	  ,-1 , // right_toe	       
	   16	  ,-1 };  // left_toe 

	  return cvt_tb[id][0];
  }

  int convert17Idto15Id(int id)
  {
	  int cvt_tb[17][2] =
	  { 0	  , 0 , // root	          
	  1	  , 1 , // chest	        
	  2	  , 2 , // neck	          
	  3	  , 6 , // right_shoulder 
	  4	  , 7 , // right_elbow	  
	  5	  , 8 , // right_wrist	  
	  6	  ,12 , // left_shoulder	
	  7	  ,13 , // left_elbow	    
	  8	  ,14 , // left_wrist	    
	  9	  , 3 , // right_hip	    
	  10	, 4 , // right_knee	    
	  11	, 5 , // right_ankle	  
	  12	,-1 , // right_toe	    
	  13	,9  , // left_hip	      
	  14	,10 , // left_knee	    
	  15	,11 , // left_ankle	    
	  16	,-1  // left_toe	      
	  };
	  return cvt_tb[id][1];
  }

  int get_parent_id(int child_id)
  {
	  const int parent_arr[17][2] =
	  { 0 , -1 ,
	  1 , 0  ,
	  2 , 1  ,
	  3 ,1   ,
	  4 , 3  ,
	  5 , 4  ,
	  6 ,1   ,
	  7 , 6  ,
	  8 , 7  ,
	  9 , 0  ,
	  10 , 9  ,
	  11 , 10 ,
	  12 , 11 ,
	  13 , 0  ,
	  14 , 13 ,
	  15 , 14 ,
	  16 , 15 };
	  return parent_arr[child_id][1];
  }


  

  void get_node_childs(int node_id, vector<int> &childs)
  {
	  int i;
	  childs.clear();
	  for (i = 0; i < 17; i++)
	  {
		  if (get_parent_id(i) == node_id)
			  childs.push_back(i);
	  }
  }

  Matrix4D  JointWorldTrans(int joint_id)
  {
	  const int  gInvalidJointID = -1;
	  Matrix4D m = Matrix4D::Identity();
	  int curr_id = joint_id;
	  assert(joint_id != gInvalidIdx); // invalid joint
	  while (curr_id != gInvalidJointID)
	  {
		  Matrix4D child_parent_mat = joint_local_transform[curr_id];
		  m = child_parent_mat * m;
		  curr_id = get_parent_id(curr_id);
	  }

	  return m;
  }
  void  DrawCharShapes(const Vector4D& fill_tint, const Vector4D& line_col)
  {

	  const auto& shape_defs = gCharModel.mDrawShapeDefs;
	  size_t num_shapes = shape_defs.rows();

	  cDrawUtil::SetLineWidth(1);
	  for (int i = 0; i < num_shapes; ++i)
	  {
		  cKinTree::tDrawShapeDef curr_def = shape_defs.row(i);
		  int parent_joint = cKinTree::GetDrawShapeParentJoint(curr_def);
		  parent_joint = convert15Idto17Id(parent_joint);
		  Matrix4D parent_world_trans =
			  JointWorldTrans(parent_joint);
		  cShape::eShape shape = static_cast<cShape::eShape>((int)curr_def[cKinTree::eDrawShapeShape]);
		  switch (shape)
		  {
		  case cShape::eShapeBox:
			  gCharModel.DrawShapeBox(curr_def, parent_world_trans, fill_tint, line_col);
			  break;
		  case cShape::eShapeCapsule:
			  gCharModel.DrawShapeCapsule(curr_def, parent_world_trans, fill_tint, line_col);
			  break;
		  case cShape::eShapeSphere:
			  gCharModel.DrawShapeSphere(curr_def, parent_world_trans, fill_tint, line_col);
			  break;
		  case cShape::eShapeCylinder:
			  gCharModel.DrawShapeCylinder(curr_def, parent_world_trans, fill_tint, line_col);
			  break;
		  default:
			  break;
		  }

	  }
  }

  void DrawHumanTree(int joint_id)//const Matrix4D *joint_local_trs, 
  {
	  double link_width = 4.0;
	  if (joint_id != -1)
	  {
		  int human_15_id = convert17Idto15Id(joint_id);
		  if (human_15_id != -1)
		  {
			  Vector4D attach_pt = joint_AttachOffset[joint_id];

			  Vector4D attach_dir = attach_pt.normalized();
			  const Vector4D up = Vector4D(0, 1, 0, 0);
			  Matrix4D rot_mat = cMathUtil::DirToRotMat(attach_dir, up);

			  //--------------------------------------------------
			  double len = attach_pt.norm();			   
			  Vector4D pos = (len / 2) * attach_dir;
			  Vector4D size = Vector4D(link_width, len, link_width, 0);

			  // draw link
			  cDrawUtil::PushMatrixView();
			  cDrawUtil::Translate(pos);
			  cDrawUtil::MultMatrixView(rot_mat);
			  cDrawUtil::Rotate(0.5 * M_PI, Vector4D(1, 0, 0, 0));

			 // cDrawUtil::SetColor(Vector4D(fill_col[0], fill_col[1], fill_col[2], fill_col[3]));
			  cDrawUtil::DrawBox(Vector4D::Zero(), size, cDrawUtil::eDrawSolid);

			  cDrawUtil::PopMatrixView();

			  //--------------------------------------------------
		  }
	  }

	  cDrawUtil::PushMatrixView();
	  Matrix4D m =  joint_local_transform[joint_id];
	  cDrawUtil::MultMatrixView(m);
	  cDrawUtil::DrawSphere(5);
	  vector<int> children;
	  get_node_childs(joint_id, children);
	  for (int i = 0; i < children.size(); ++i)
	  {
		  int child_id = children[i];
		  DrawHumanTree(child_id);//joint_local_trs, 
	  }
	  cDrawUtil::PopMatrixView();
  }

  Character()
    : vbo(0)
    , tbo(0)
    , ntri(66918)
    , nvtx(11200)
    , phase(0)
    , strafe_amount(0)
    , strafe_target(0)
    , crouched_amount(0) 
    , crouched_target(0) 
    , responsive(0) {}
    
  ~Character() {
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; }
  }
  



  void load(const char* filename_v, const char* filename_t, const char* filename_p, const char* filename_r) {
    
    printf("Read Character '%s %s'\n", filename_v, filename_t);
    
    if (vbo != 0) { vbo = 0; }
    if (tbo != 0) { tbo = 0; }
    
   
    
    FILE *f;
    
     
	
    for (int i = 0; i < JOINT_NUM; i++)
		{ joint_parents[i] = parent_arr[i][1];//(int)fparents[i]; 
	      
	     }
     
    
  }
  
  void forward_kinematics() {

    for (int i = 0; i < JOINT_NUM; i++) {
      
      int j = joint_parents[i];
      while (j != -1) {
        
        j = joint_parents[j];
      }
       
    }
    
  }
  
};


struct Heightmap {
  
  float hscale;
  float vscale;
  float offset;
  std::vector<std::vector<float>> data;
  std::vector<glm::vec3> posns;
  std::vector<glm::vec3> norms;
  std::vector<float> aos;
  GLuint vbo;
  GLuint tbo;
  int w,h;
  Heightmap()
    : hscale(3.937007874)
    //, vscale(3.937007874)
    , vscale(3.0)
    , offset(0.0)
    , vbo(0)
    , tbo(0) {}
  
  ~Heightmap() {
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; } 
  }
  
  void load(const char* filename, float multiplier) {
    
    vscale = multiplier * vscale;
    
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; }
    
    //_ glGenBuffers(1, &vbo);
    //_ glGenBuffers(1, &tbo);
    
    data.clear();
    
    std::ifstream file(filename);
    
    std::string line;
    while (std::getline(file, line)) {
      std::vector<float> row;
      std::istringstream iss(line);
      while (iss) {
        float f;
        iss >> f;
        row.push_back(f);
      }
      data.push_back(row);
    }
    
    w = data.size();
    h = data[0].size();
    
    offset = 0.0;
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      offset += data[x][y];
    }
    offset /= w * h;
    
    printf("Loaded Heightmap '%s' (%i %i)\n", filename, (int)w, (int)h);
    
    //glm::vec3* posns = (glm::vec3*)malloc(sizeof(glm::vec3) * w * h);
    //glm::vec3* norms = (glm::vec3*)malloc(sizeof(glm::vec3) * w * h);
	posns.resize(w * h);
	norms.resize(w * h);
	aos.resize(w * h); //float* aos   = (float*)malloc(sizeof(float) * w * h);
    
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      float cx = hscale * x, cy = hscale * y, cw = hscale * w, ch = hscale * h;
      posns[x+y*w] = glm::vec3(cx - cw/2, sample(glm::vec2(cx-cw/2, cy-ch/2)), cy - ch/2);
    }
    
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      norms[x+y*w] = (x > 0 && x < w-1 && y > 0 && y < h-1) ?
        glm::normalize(glm::mix(
          glm::cross(
            posns[(x+0)+(y+1)*w] - posns[x+y*w],
            posns[(x+1)+(y+0)*w] - posns[x+y*w]),
          glm::cross(
            posns[(x+0)+(y-1)*w] - posns[x+y*w],
            posns[(x-1)+(y+0)*w] - posns[x+y*w]), 0.5)) : glm::vec3(0,1,0);
    }


    char ao_filename[512];
    memcpy(ao_filename, filename, strlen(filename)-4);
    ao_filename[strlen(filename)-4] = '\0';
    strcat(ao_filename, "_ao.txt");
    
    srand(0);

    FILE* ao_file = fopen(ao_filename, "r");
    bool ao_generate = false;
    if (ao_file == NULL || ao_generate) {
      ao_file = fopen(ao_filename, "w");
      //ao_generate = true;
    }
   
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {     
        fscanf(ao_file, y == h-1 ? "%f\n" : "%f ", &aos[x+y*w]);
    }
    
    fclose(ao_file);
    
  }
  
  float sample(glm::vec2 pos) {
  
    int w = data.size();
    int h = data[0].size();
    
    pos.x = (pos.x/hscale) + w/2;
    pos.y = (pos.y/hscale) + h/2;
    
    float a0 = fmod(pos.x, 1.0);
    float a1 = fmod(pos.y, 1.0);
    
    int x0 = (int)std::floor(pos.x), x1 = (int)std::ceil(pos.x);
    int y0 = (int)std::floor(pos.y), y1 = (int)std::ceil(pos.y);
    
    x0 = x0 < 0 ? 0 : x0; x0 = x0 >= w ? w-1 : x0;
    x1 = x1 < 0 ? 0 : x1; x1 = x1 >= w ? w-1 : x1;
    y0 = y0 < 0 ? 0 : y0; y0 = y0 >= h ? h-1 : y0;
    y1 = y1 < 0 ? 0 : y1; y1 = y1 >= h ? h-1 : y1;
    
    float s0 = vscale * (data[x0][y0] - offset);
    float s1 = vscale * (data[x1][y0] - offset);
    float s2 = vscale * (data[x0][y1] - offset);
    float s3 = vscale * (data[x1][y1] - offset);
    
    return (s0 * (1-a0) + s1 * a0) * (1-a1) + (s2 * (1-a0) + s3 * a0) * a1;
  
  }
  
};

#endif
