#ifndef HUMAN_MOTION_HEAD_H
#define HUMAN_MOTION_HEAD_H

#include "json/json.h"
#include "KinTree.h"
 

class cMotion
{
public:
	enum eFrameParams
	{
		eFrameTime,
		eFrameMax
	};
	typedef Eigen::VectorXd tFrame;
 

	struct tParams
	{
		std::string mMotionFile;
		std::vector<int> mRightJoints;
		std::vector<int> mLeftJoints;

		inline tParams();
	};

	inline cMotion();
	 
	 void inline Clear();
	 bool inline Load(const tParams& params);
	 int inline GetNumFrames() const;
	 tFrame inline GetFrame(int i) const;
	  void inline CalcFrame(double time, tFrame& out_frame, bool force_mirror = false) const;	 
	 double inline GetDuration() const;	 
	 int inline CalcCycleCount(double time) const;
	 void inline CalcIndexPhase(double time, int& out_idx, double& out_phase) const;	 
	 
	 
protected:
	 tParams mParams;
	Eigen::MatrixXd mFrames;
	 
	 bool inline LoadJson(const Json::Value& root);
	 bool inline LoadJsonFrames(const Json::Value& root, Eigen::MatrixXd& out_frames) const;
	 bool inline ParseFrameJson(const Json::Value& root, Eigen::VectorXd& out_frame) const;
	 bool inline LoadJsonJoints(const Json::Value& root, std::vector<int>& out_right_joints, std::vector<int>& out_left_joints) const;	 
	 void inline RecordFrameTime(Eigen::MatrixXd& frames) const;
	 int inline GetFrameSize() const;
	 
};

cMotion::tParams::tParams()
{
	mMotionFile = "";
	mRightJoints.clear();
	mLeftJoints.clear();
};

cMotion::cMotion()
{
	Clear();
}

void cMotion::Clear()
{
	mFrames.resize(0, 0);	 
	mParams = tParams();
}

bool cMotion::Load(const tParams& params)
{
	Clear();

	mParams = params;
	std::ifstream f_stream(mParams.mMotionFile);
	Json::Value root;
	Json::Reader reader;
	bool succ = reader.parse(f_stream, root);
	f_stream.close();

	succ = LoadJson(root);
	RecordFrameTime(mFrames);
	return succ;
}



int cMotion::GetNumFrames() const
{
	return static_cast<int>(mFrames.rows());
}

int cMotion::GetFrameSize() const
{
	return static_cast<int>(mFrames.cols());
}


cMotion::tFrame cMotion::GetFrame(int i) const
{
	int frame_size = GetFrameSize();
	return mFrames.row(i).segment(eFrameMax, frame_size - eFrameMax);
}

 

void cMotion::CalcFrame(double time, tFrame& out_frame, bool force_mirror /*=false*/) const
{
	int idx;
	double phase;
	CalcIndexPhase(time, idx, phase);
	out_frame = GetFrame(idx);
}


bool cMotion::LoadJson(const Json::Value& root)
{
	bool succ = true;
	 	 
	if (mParams.mRightJoints.size() == 0 && mParams.mLeftJoints.size() == 0)
	{
		succ &= LoadJsonJoints(root, mParams.mRightJoints, mParams.mLeftJoints);
	}
	 
	if (!root["Frames"].isNull())
	{
		succ &= LoadJsonFrames(root["Frames"], mFrames);
	}
	return succ;
}

 

bool cMotion::LoadJsonFrames(const Json::Value& root, Eigen::MatrixXd& out_frames) const
{
	bool succ = true;

	assert(root.isArray());
	int num_frames = root.size();

	int data_size = 0;
	if (num_frames > 0)
	{
		int idx0 = 0;
		Json::Value frame_json = root.get(idx0, 0);
		data_size = frame_json.size();
		out_frames.resize(num_frames, data_size);
	}

	for (int f = 0; f < num_frames; ++f)
	{
		Eigen::VectorXd curr_frame;
		succ &= ParseFrameJson(root.get(f, 0), curr_frame);
		if (succ)
		{
			assert(mFrames.cols() == curr_frame.size());
			out_frames.row(f) = curr_frame;
		}

	}
	return succ;
}

bool cMotion::ParseFrameJson(const Json::Value& root, Eigen::VectorXd& out_frame) const
{
	bool succ = false;
	if (root.isArray())
	{
		int data_size = root.size();
		out_frame.resize(data_size);
		for (int i = 0; i < data_size; ++i)
		{
			Json::Value json_elem = root.get(i, 0);
			out_frame[i] = json_elem.asDouble();
		}

		succ = true;
	}
	return succ;
}

bool cMotion::LoadJsonJoints(const Json::Value& root, std::vector<int>& out_right_joints, std::vector<int>& out_left_joints) const
{
	out_right_joints.clear();
	out_left_joints.clear();

	return true;
}

void cMotion::RecordFrameTime(Eigen::MatrixXd& frames) const
{
	int frame_size = GetFrameSize();
	int num_frames = static_cast<int>(frames.rows());
	double curr_time = 0;// gMinTime;

	for (int f = 0; f < num_frames; ++f)
	{
		auto curr_frame = frames.row(f);
		double duration = curr_frame(0, eFrameTime);
		curr_frame(0, eFrameTime) = curr_time;
		curr_time += duration;
	}
}

double cMotion::GetDuration() const
{
	int num_frames = GetNumFrames();
	double max_time = mFrames(num_frames - 1, eFrameTime);
	return max_time;
}

int cMotion::CalcCycleCount(double time) const
{
	double dur = GetDuration();
	double phases = time / dur;
	int count = static_cast<int>(std::floor(phases));
	return count;
}

void cMotion::CalcIndexPhase(double time, int& out_idx, double& out_phase) const
{
	double max_time = GetDuration();
	double gMinTime = 0;
	 
	if (time <= gMinTime)
	{
		out_idx = 0;
		out_phase = 0;
		return;
	}

	out_idx = 0;
	
	int cycle_count = CalcCycleCount(time);
	time -= cycle_count * GetDuration();

	const Eigen::VectorXd& frame_times = mFrames.col(eFrameTime);
	auto it = std::upper_bound(frame_times.data(), frame_times.data() + frame_times.size(), time);
	out_idx = static_cast<int>(it - frame_times.data() - 1);
	//upper_bound(起始地址，结束地址，要查找的数值) 返回的是数值 最后一个 出现的位置 
}

 
#endif 