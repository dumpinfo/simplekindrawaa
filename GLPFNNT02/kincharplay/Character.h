#ifndef CHARACTER_HEAD_H
#define CHARACTER_HEAD_H

#include <memory>
#include "KinTree.h"
#include <assert.h>
#include <functional>
#include "json/json.h"
#include "DrawUtil.h"
#include <vector>



using namespace std;

class cCharacter
{
public:
	
	 // bool inline Init(const std::string& char_file, bool load_draw_shapes);
	void inline Clear();
    bool inline LoadSkeleton(const Json::Value& root);
	 
	Eigen::MatrixXd mDrawShapeDefs;
	inline  cCharacter();
 
	int mID;
	Eigen::MatrixXd mJointMat;
	Eigen::VectorXd mPose;
	Eigen::VectorXd mPose0;
	vector<string> ktree_names;

public:
	bool inline  Init(const std::string& char_file);
	void inline SetOriginPos(const Vector4D& origin);
	void inline MoveOrigin(const Vector4D& delta); 

	Vector4D mOrigin;
	tQuaternion mOriginRot;
	//-------------------------------------------------------------------
	void  inline DrawCharShapes(  const Vector4D& fill_tint, const Vector4D& line_col);
	void  inline DrawShapeBox(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
		const Vector4D& fill_tint, const Vector4D& line_col);
	void  inline DrawShapeCapsule(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
		const Vector4D& fill_tint, const Vector4D& line_col);
	void  inline DrawShapeSphere(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
		const Vector4D& fill_tint, const Vector4D& line_col);
	void  inline DrawShapeCylinder(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
		const Vector4D& fill_tint, const Vector4D& line_col);	 
	//--------------------------------------------------------------------
	void inline DrawTree(const Eigen::MatrixXd& joint_desc, const Eigen::VectorXd& pose, int joint_id, double link_width,
		const Vector4D& fill_col, const Vector4D& line_col);
	 
};


cCharacter::cCharacter()
{
	mID = gInvalidIdx;
	mOrigin.setZero();
	mOriginRot.setIdentity();
	 
}

bool cCharacter::Init(const std::string& char_file)
{
	Clear();

	bool succ = true;
	if (char_file != "")
	{
		std::ifstream f_stream(char_file);
		Json::Reader reader;
		Json::Value root;
		succ = reader.parse(f_stream, root);
		f_stream.close();
		succ = LoadSkeleton(root["Skeleton"]);

	}

	succ &= cKinTree::LoadDrawShapeDefs(char_file, mDrawShapeDefs);
	return succ;
}

void cCharacter::Clear()
{
	mPose.resize(0);
	mPose0.resize(0);	 
	mDrawShapeDefs.resize(0, 0);
}

 

bool cCharacter::LoadSkeleton(const Json::Value& root)
{
	return cKinTree::Load(root, mJointMat);
}


void cCharacter::SetOriginPos(const Vector4D& origin)
{
	Vector4D delta = origin - mOrigin;
	MoveOrigin(delta);
	mOrigin = origin; 
}

void cCharacter::MoveOrigin(const Vector4D& delta)
{
	mOrigin += delta;

	Vector4D root0 = cKinTree::GetRootPos(mJointMat, mPose0);
	root0 += delta;
	cKinTree::SetRootPos(mJointMat, root0, mPose0);

	Vector4D root = cKinTree::GetRootPos(mJointMat, mPose);
	root += delta;
	cKinTree::SetRootPos(mJointMat, root, mPose);
}

void cCharacter::DrawTree(const Eigen::MatrixXd& joint_desc, const Eigen::VectorXd& pose, int joint_id, double link_width,
	const Vector4D& fill_col, const Vector4D& line_col)
{
	const double node_radius = link_width;
	//cout << joint_desc;

	if (joint_id != -1)//
	{
		bool has_parent = cKinTree::HasParent(joint_desc, joint_id);
		if (has_parent)
		{
			Vector4D attach_pt = cKinTree::GetAttachPt(joint_desc, joint_id);

			Vector4D OffSet = cKinTree::GetAttachPt(joint_desc, joint_id);

			double len = attach_pt.norm();
			if(DBG_TREE_INFO)
			  printf("joint length (to %s)%8.3lf,", cKinTree::query_joint_idname(joint_id).c_str(),len);

			Vector4D attach_dir = attach_pt.normalized();
			const Vector4D up = Vector4D(0, 1, 0, 0);
			Matrix4D rot_mat = cMathUtil::DirToRotMat(attach_dir, up);

			Vector4D pos = (len / 2) * attach_dir;
			Vector4D size = Vector4D(link_width, len, link_width, 0);

			cDrawUtil::PushMatrixView();
			cDrawUtil::SetColor(Vector4D(1.0, 0, 0, 1));
			cDrawUtil::Translate(pos);
			cDrawUtil::DrawSphere(0.035);
			cDrawUtil::PopMatrixView();

			// draw link
			cDrawUtil::PushMatrixView();
			cDrawUtil::Translate(pos);
			cDrawUtil::MultMatrixView(rot_mat);
			cDrawUtil::Rotate(0.5 * M_PI, Vector4D(1, 0, 0, 0));

			cDrawUtil::SetColor(Vector4D(fill_col[0], fill_col[1], fill_col[2], fill_col[3]));
			cDrawUtil::DrawBox(Vector4D::Zero(), size, cDrawUtil::eDrawSolid);

			if (line_col[3] > 0)
			{
				cDrawUtil::SetColor(Vector4D(line_col[0], line_col[1], line_col[2], line_col[3]));
				cDrawUtil::DrawBox(Vector4D::Zero(), size, cDrawUtil::eDrawWireSimple);
			}

			cDrawUtil::PopMatrixView();
             /*
			Vector4D OffSetTrans = OffSet * rot_mat ;

			cDrawUtil::PushMatrixView();
			cDrawUtil::Translate(OffSetTrans);
			cDrawUtil::DrawSphere(0.05);
			cDrawUtil::PopMatrixView();*/

		}
		
		//#########################POSITION_DBG_TEST######################################
		vector<int> children_dbg;
		cKinTree::FindChildren(joint_desc, joint_id, children_dbg);
		for (int i = 0; i < children_dbg.size(); ++i)
		{
			int child_id = children_dbg[i];
			Vector4D attach_pt = cKinTree::GetAttachPt(joint_desc, child_id);
			Matrix4D m = cKinTree::ChildParentTrans(joint_desc, pose, joint_id);
			Vector4D offset = m.col(3);
			m(0, 3) = 0;
			m(1, 3) = 0;
			m(2, 3) = 0;
			attach_pt = attach_pt * 0.7;
			attach_pt = m * attach_pt;
			attach_pt += offset;

			cDrawUtil::PushMatrixView();
			cDrawUtil::Translate(attach_pt);
			cDrawUtil::SetColor(Vector4D(0, 1.0, 0, 1));
			cDrawUtil::DrawSphere(0.04);
			cDrawUtil::PopMatrixView();
			//DrawTree(joint_desc, pose, child_id, link_width, fill_col, line_col);
		}
		//################################################################################

		cDrawUtil::PushMatrixView();
		Matrix4D m = cKinTree::ChildParentTrans(joint_desc, pose, joint_id);
		cDrawUtil::MultMatrixView(m);

		// draw node
		cDrawUtil::SetColor(Vector4D(fill_col[0] * 0.25, fill_col[1] * 0.25, fill_col[2] * 0.25, fill_col[3]));
		cDrawUtil::DrawSphere(node_radius);

		



		vector<int> children;
		cKinTree::FindChildren(joint_desc, joint_id, children);
		for (int i = 0; i < children.size(); ++i)
		{
			int child_id = children[i];
			DrawTree(joint_desc, pose, child_id, link_width, fill_col, line_col);
		}

		cDrawUtil::PopMatrixView();
	}
}

void cCharacter::DrawCharShapes(const Vector4D& fill_tint, const Vector4D& line_col)
{

	const auto& shape_defs = mDrawShapeDefs;
	size_t num_shapes = shape_defs.rows();

	cDrawUtil::SetLineWidth(1);
	for (int i = 0; i < num_shapes; ++i)
	{
		cKinTree::tDrawShapeDef curr_def = shape_defs.row(i);
		int parent_joint = cKinTree::GetDrawShapeParentJoint(curr_def);
		Matrix4D parent_world_trans =  
			cKinTree::JointWorldTrans( mJointMat,  mPose, parent_joint);

		cShape::eShape shape = static_cast<cShape::eShape>((int)curr_def[cKinTree::eDrawShapeShape]);
	   switch (shape)
	  {
	  case cShape::eShapeBox:
		DrawShapeBox(curr_def, parent_world_trans, fill_tint, line_col);
		break;
	  case cShape::eShapeCapsule:
		DrawShapeCapsule(curr_def, parent_world_trans, fill_tint, line_col);
		break;
	  case cShape::eShapeSphere:
		DrawShapeSphere(curr_def, parent_world_trans, fill_tint, line_col);
		break;
	  case cShape::eShapeCylinder:
		DrawShapeCylinder(curr_def, parent_world_trans, fill_tint, line_col);
		break;
	  default:
		break;
	   }
		
	}
}


void cCharacter::DrawShapeBox(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
	const Vector4D& fill_tint, const Vector4D& line_col)
{
	double theta = 0;
	Vector4D euler = cKinTree::GetDrawShapeAttachTheta(def);
	int parent_joint = cKinTree::GetDrawShapeParentJoint(def);
	Vector4D attach_pt = cKinTree::GetDrawShapeAttachPt(def);
	Vector4D col = cKinTree::GetDrawShapeColor(def);
	Vector4D size = Vector4D(def[cKinTree::eDrawShapeParam0], def[cKinTree::eDrawShapeParam1], def[cKinTree::eDrawShapeParam2], 0);
	col = col.cwiseProduct(fill_tint);

	cDrawUtil::PushMatrixView();
	cDrawUtil::MultMatrixView(parent_world_trans);
	cDrawUtil::Translate(attach_pt*100.0);;
	cDrawUtil::Rotate(euler);

	cDrawUtil::SetColor(col);
	cDrawUtil::DrawBox(Vector4D::Zero(), size, cDrawUtil::eDrawSolid);

	if (line_col[3] > 0)
	{
		cDrawUtil::SetColor(line_col);
		cDrawUtil::DrawBox(Vector4D::Zero(), size, cDrawUtil::eDrawWireSimple);
	}

	cDrawUtil::PopMatrixView();
}

void cCharacter::DrawShapeCapsule(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
	const Vector4D& fill_tint, const Vector4D& line_col)
{
	double theta = 0;
	Vector4D axis = Vector4D(0, 0, 1, 0);
	cKinTree::GetDrawShapeRotation(def, axis, theta);
	int parent_joint = cKinTree::GetDrawShapeParentJoint(def);
	Vector4D attach_pt = cKinTree::GetDrawShapeAttachPt(def);
	Vector4D col = cKinTree::GetDrawShapeColor(def);
	Vector4D size = Vector4D(def[cKinTree::eDrawShapeParam0], def[cKinTree::eDrawShapeParam1], 0, 0);
	col = col.cwiseProduct(fill_tint);

	double r = 0.5 * size[0] * 100.0;
	double h = size[1] * 100.0;

	cDrawUtil::PushMatrixView();
	cDrawUtil::MultMatrixView(parent_world_trans);
	cDrawUtil::Translate(attach_pt*100.0);;
	cDrawUtil::Rotate(theta, axis);

	cDrawUtil::SetColor(col);
	cDrawUtil::DrawCapsule(r, h, cDrawUtil::eDrawSolid);

	if (line_col[3] > 0)
	{
		cDrawUtil::SetColor(line_col);
		cDrawUtil::DrawCapsule(r, h, cDrawUtil::eDrawWireSimple);
	}

	cDrawUtil::PopMatrixView();
}


void cCharacter::DrawShapeSphere(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
	const Vector4D& fill_tint, const Vector4D& line_col)
{
	double theta = 0;
	Vector4D axis = Vector4D(0, 0, 1, 0);
	cKinTree::GetDrawShapeRotation(def, axis, theta);
	int parent_joint = cKinTree::GetDrawShapeParentJoint(def);
	Vector4D attach_pt = cKinTree::GetDrawShapeAttachPt(def);
	Vector4D col = cKinTree::GetDrawShapeColor(def);
	Vector4D size = Vector4D(def[cKinTree::eDrawShapeParam0], def[cKinTree::eDrawShapeParam1], 0, 0);
	col = col.cwiseProduct(fill_tint);

	double r = 0.5 * size[0] * 100.0;

	cDrawUtil::PushMatrixView();
	cDrawUtil::MultMatrixView(parent_world_trans);
	cDrawUtil::Translate(attach_pt*100.0);;
	cDrawUtil::Rotate(theta, axis);

	cDrawUtil::SetColor(col);
	cDrawUtil::DrawSphere(r, cDrawUtil::eDrawSolid);

	if (line_col[3] > 0)
	{
		cDrawUtil::SetColor(line_col);
		cDrawUtil::DrawSphere(r, cDrawUtil::eDrawWireSimple);
	}

	cDrawUtil::PopMatrixView();
}


void cCharacter::DrawShapeCylinder(const cKinTree::tDrawShapeDef& def, const Matrix4D& parent_world_trans,
	const Vector4D& fill_tint, const Vector4D& line_col)
{
	double theta = 0;
	Vector4D axis = Vector4D(0, 0, 1, 0);
	cKinTree::GetDrawShapeRotation(def, axis, theta);
	int parent_joint = cKinTree::GetDrawShapeParentJoint(def);
	Vector4D attach_pt = cKinTree::GetDrawShapeAttachPt(def);
	Vector4D col = cKinTree::GetDrawShapeColor(def);
	Vector4D size = Vector4D(def[cKinTree::eDrawShapeParam0], def[cKinTree::eDrawShapeParam1], 0, 0);
	col = col.cwiseProduct(fill_tint);

	double r = 0.5 * size[0] * 100.0;
	double h = size[1] * 100.0;

	cDrawUtil::PushMatrixView();
	cDrawUtil::MultMatrixView(parent_world_trans);
	cDrawUtil::Translate(attach_pt*100.0);;
	cDrawUtil::Rotate(theta, axis);

	cDrawUtil::SetColor(col);
	cDrawUtil::DrawCylinder(r, h, cDrawUtil::eDrawSolid);

	if (line_col[3] > 0)
	{
		cDrawUtil::SetColor(line_col);
		cDrawUtil::DrawCylinder(r, h, cDrawUtil::eDrawWireSimple);
	}

	cDrawUtil::PopMatrixView();
}

#endif